<?php
    require_once('app/Mage.php'); //Path to Magento
    umask(0);
    Mage::app();

    /*
    Mage::getModel('wgmulti/warehouse_product')
        ->getCollection()
        ->addProductIdFilter(8481)
        ->delete();
    */
    
    
    $totalQty = 0.0;
    
    $quantities = array("1" => "10", "2" => "20", "3" => "30");
    
    //Go through quantities, sum them up, set quantity for each warehouse and set full quantity for the product
    foreach ($quantities as $warehouseId => $qty)
    {
        Mage::getModel('wgmulti/warehouse_product')
        	->getCollection() //get the collection
        	->addWarehouseIdFilter($warehouseId) //filter by warehouse
        	->addProductIdFilter(8481) //filter by product id
        	->getFirstItem() // if doesn't exist, return new object
        	->setWarehouseId($warehouseId) // set the warehouse id
        	->setProductId(8481) // set the product id
        	->setQty($qty) // set the quantity for the product and warehouse
        	->save(); // save it
        
        	$totalQty += $qty;
    }
   
    
    $productModel = Mage::getModel('catalog/product');
    
    //$productId = $productModel -> getIdBySku($row["BARCODE"]);  
    $product = $productModel -> load(8481); //Get product by id
    $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
    
    if ($stockItem) {
    	$stockItem
    	->setQty($totalQty)
    	->save();
    }
    
?>