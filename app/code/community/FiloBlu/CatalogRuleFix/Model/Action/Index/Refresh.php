<?php

/**
 * Catalog rule indexer
 *
 * @category    Mage
 * @package     Mage_CatalogRule
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class FiloBlu_CatalogRuleFix_Model_Action_Index_Refresh extends Mage_CatalogRule_Model_Action_Index_Refresh {

    /**
     * Run reindex
     */
    public function execute() {
        $this->_app->dispatchEvent('catalogrule_before_apply', array('resource' => $this->_resource));

        /** @var $coreDate Mage_Core_Model_Date */
        $coreDate = $this->_factory->getModel('core/date');
        $timestamp = strtotime($coreDate->date('Y-m-d'));

        foreach ($this->_app->getWebsites(false) as $website) {
            /** @var $website Mage_Core_Model_Website */
            if ($website->getDefaultStore()) {
                $this->_reindex($website, $timestamp);
            }
        }

        $this->_prepareGroupWebsite($timestamp);
        $this->_prepareAffectedProduct();
    }

}
