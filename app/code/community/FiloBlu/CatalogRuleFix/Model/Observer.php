<?php

/**
 * Catalog Price rules observer model
 *
 * @category    FiloBlu
 * @package     FiloBlu_CatalogRuleFix
 * @author      Luca Lorenzato <support@filoblu.com>
 */
class FiloBlu_CatalogRuleFix_Model_Observer extends Mage_CatalogRule_Model_Observer {

    /**
     * Apply all price rules for current date.
     * Handle cataolg_product_import_after event
     *
     * @param   Varien_Event_Observer $observer
     *
     * @return  Mage_CatalogRule_Model_Observer
     */
    public function applyAllRules($observer) {

        if (Mage::getEdition() !== Mage::EDITION_COMMUNITY) {
            return parent::applyAllRules($observer);
        }

        $dateObj = Mage::getModel('core/date');
        $resource = Mage::getResourceSingleton('catalogrule/rule');
        $resource->applyAllRulesForDateRange($resource->formatDate(mktime(0, 0, 0, $dateObj->date('m'), $dateObj->date('d'))));
        Mage::getModel('catalogrule/flag')->loadSelf()
                ->setState(0)
                ->save();

        return $this;
    }

    /**
     * Daily update catalog price rule by cron
     * Update include interval 3 days - current day - 1 days before + 1 days after
     * This method is called from cron process, cron is working in UTC time and
     * we should generate data for interval -1 day ... +1 day
     *
     * @param   Varien_Event_Observer $observer
     *
     * @return  Mage_CatalogRule_Model_Observer
     */
    public function dailyCatalogUpdate($observer) {

        if (Mage::getEdition() !== Mage::EDITION_COMMUNITY) {
            return parent::dailyCatalogUpdate($observer);
        }

        Mage::getModel('catalogrule/rule')->applyAll();
        Mage::getModel('catalogrule/flag')->loadSelf()
                ->setState(0)
                ->save();

        return $this;
    }

}
