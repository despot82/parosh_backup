<?php

/**
 * Catalog Price rules observer model
 *
 * @category    FiloBlu
 * @package     FiloBlu_CatalogRuleFix
 * @author      Luca Lorenzato <support@filoblu.com>
 */
class FiloBlu_CatalogRuleFix_Model_Rule extends Mage_CatalogRule_Model_Rule {

    /**
     * Invalidate related cache types
     *
     * @return Mage_CatalogRule_Model_Rule
     */
    protected function _invalidateCache() {
        parent::_invalidateCache();

        if (Mage::helper('core')->isModuleEnabled('Lesti_Fpc')) {

            $frontend = Mage::getSingleton('fpc/fpc');

            if ($frontend) {
                $frontend = $frontend->getFrontend();
                $backend = $frontend->getBackend();
                $backend->clean();
            }
        }

        return $this;
    }

}
