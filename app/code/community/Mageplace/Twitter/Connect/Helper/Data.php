<?php
/**
 * Mageplace Twitter Connector
 *
 * @category	Mageplace_Twitter
 * @package		Mageplace_Twitter_Connect
 * @copyright	Copyright (c) 2011 Mageplace. (http://www.mageplace.com)
 * @license		http://www.mageplace.com/disclaimer.html
 */

 if(Mage::helper('twitterconnect/version')->isEE()) {
     class Mageplace_Twitter_Connect_Helper_Data extends Mageplace_Twitter_Connect_Helper_Enterprise
    {	
    }
} else {	
    class Mageplace_Twitter_Connect_Helper_Data extends Mageplace_Twitter_Connect_Helper_Community
    {	
    }
}
 
 

