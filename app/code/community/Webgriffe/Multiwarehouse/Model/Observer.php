<?php
class Webgriffe_Multiwarehouse_Model_Observer
{

    /**
     * event: catalog_product_save_before
     * This handler fires when product is changed (saved, immediately before it is saved)
     * @param Varien_Event_Observer $observer
     */
    public function handleMultipleQuantitiesPost(Varien_Event_Observer $observer)
    {
        if (!Mage::app()->getRequest()->isPost()) {
            return;
        }

        /** @var Mage_Catalog_Model_Product $product */
        $product = $observer->getProduct();

        $post = Mage::app()->getRequest()->getPost();

        /*
         * If multiple quantity was disabled (multiwarehouse checkbox is unchecked), delete the DB entry for 
         * multiwarehouse for this product
         *   
         */
        if ($post['wgmulti_original_use_multiple_qty'] == 1 && $post['wgmulti_use_multiple_qty'] == 0)
        {
            Mage::getModel('wgmulti/warehouse_product')
                ->getCollection()
                ->addProductIdFilter($product->getId())
                ->delete();
        }

        // if multiple quantity was enabled, loop them and update DB tables
        if ($post['wgmulti_use_multiple_qty'] == 1)
        {
            $totalQty = 0.0;
            foreach ($post['wgmultiqty'] as $warehouseId => $qty)
            {
                Mage::getModel('wgmulti/warehouse_product')
                    ->getCollection() //get the collection
                    ->addWarehouseIdFilter($warehouseId) //filter by warehouse
                    ->addProductIdFilter($product->getId()) //filter by product id
                    ->getFirstItem() // if doesn't exist, return new object
                    ->setWarehouseId($warehouseId) // set the warehouse id
                    ->setProductId($product->getId()) // set the product id
                    ->setQty($qty) // set the quantity for the product and warehouse
                    ->save(); // save it
                    
                $totalQty += $qty;                     
            }
            
            $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
            if ($stockItem) {
                $stockItem
                    ->setQty($totalQty)
                    ->save();
            }
        }
    }

    /**
     * event: sales_model_service_quote_submit_success
     */
    public function decrementQuantities(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getOrder();

        /** @var Mage_Sales_Model_Order_Item $item */
        foreach ($order->getAllItems() as $item)
        {
            $additionalData = array();
            $serializedAdditionalData = $item->getAdditionalData();
            if (!empty($serializedAdditionalData)) {
                
            	//Fetch some other additional data, that may exist, if it does, preserve it here in a variable
            	$additionalData = unserialize($serializedAdditionalData); 
            }

            $orderedQty = $item->getQtyOrdered();

            /** @var Webgriffe_Multiwarehouse_Model_Resource_Warehouse_Product_Collection $warehouseProducts */
            $warehouseProducts = Mage::getModel('wgmulti/warehouse_product')
                ->getCollection()
                ->addProductIdFilter($item->getProductId())
                ->addWarehousePositionOrder();

            foreach ($warehouseProducts as $warehouseProduct)
            {
                if ($warehouseProduct->getQty() >= $orderedQty) {
                    $warehouseProduct->setQty($warehouseProduct->getQty()-$orderedQty);
                    $additionalData[$warehouseProduct->getWarehouseId()] = $orderedQty;
                    break;
                }
                
                $additionalData[$warehouseProduct->getWarehouseId()] = $warehouseProduct->getQty();
                $orderedQty -= $warehouseProduct->getQty();
                $warehouseProduct->setQty(0);
            }

            $item->setAdditionalData(serialize($additionalData))->save();

            $warehouseProducts->save();
        } //END foreach getAllItems()
    }
}