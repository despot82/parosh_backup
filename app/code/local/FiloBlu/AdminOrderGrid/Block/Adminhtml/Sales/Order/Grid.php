<?php
/**
 * @category   FiloBlu
 * @package    FiloBlu_AdminOrderGrid
 * @author     Francesco Zoccarato <francesco@filoblu.com>
 */
class FiloBlu_AdminOrderGrid_Block_Adminhtml_Sales_Order_Grid extends Mage_Adminhtml_Block_Sales_Order_Grid
{
    
//    protected function _prepareCollection() {
//        $collection = parent::_prepareCollection();
//        $collection->getSelect()->join('sales_flat_order', 'main_table.entity_id = sales_flat_order.entity_id', array('coupon_code'));
//        
////        $subSelect = $collection->getResource()->getReadConnection()->select()
////        ->from(array('sales_flat_order'), array('entity_id', 'coupon_code'));
////
////        $collection->getSelect()
////        ->join(array('order' => $subSelect), '`order`.entity_id = `main_table`.entity_id',array('coupon_code'));
//    
//        $this->setCollection($collection);
//        return $collection;
//        
//        $collection = Mage::getResourceModel($this->_getCollectionClass());
//        $collection->getSelect()->joinLeft(array('order' => Mage::getModel('core/resource')->getTableName('sales/order')),
//            'order.entity_id=main_table.entity_id',array('coupon_code' => 'coupon_code'));
//        $this->setCollection($collection);
//        return parent::_prepareCollection();
//        
//    }
    
    protected function _prepareColumns()
    {

        $this->addColumn('real_order_id', array(
            'header'=> Mage::helper('sales')->__('Order #'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'increment_id',
        ));

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_id', array(
                'header'    => Mage::helper('sales')->__('Purchased From (Store)'),
                'index'     => 'store_id',
                'type'      => 'store',
                'store_view'=> true,
                'display_deleted' => true,
            ));
        }

        $this->addColumn('created_at', array(
            'header' => Mage::helper('sales')->__('Purchased On'),
            'index' => 'created_at',
            'type' => 'datetime',
            'width' => '100px',
        ));

        $this->addColumn('billing_name', array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'index' => 'billing_name',
        ));

        $this->addColumn('shipping_name', array(
            'header' => Mage::helper('sales')->__('Ship to Name'),
            'index' => 'shipping_name',
        ));

        $this->addColumn('base_grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Base)'),
            'index' => 'base_grand_total',
            'type'  => 'currency',
            'currency' => 'base_currency_code',
        ));

        $this->addColumn('grand_total', array(
            'header' => Mage::helper('sales')->__('G.T. (Purchased)'),
            'index' => 'grand_total',
            'type'  => 'currency',
            'currency' => 'order_currency_code',
        ));

        $this->addColumn('status', array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            'type'  => 'options',
            'width' => '70px',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));
        
        $this->addColumn('coupon', array(
            'header' => Mage::helper('sales')->__('Coupon'),
//            'index' => 'coupon',
            'renderer' => 'FiloBlu_AdminOrderGrid_Block_Adminhtml_Sales_Order_Renderer_Coupon',
            'type'  => 'text',
            'escape' => true,
            'sortable' => false,
            'filter' => false,
//            'filter_index' => 'order.coupon',       
        ));
        
        if (Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/view')) {
            $this->addColumn('action',
                array(
                    'header'    => Mage::helper('sales')->__('Action'),
                    'width'     => '50px',
                    'type'      => 'action',
                    'getter'     => 'getId',
                    'actions'   => array(
                        array(
                            'caption' => Mage::helper('sales')->__('View'),
                            'url'     => array('base'=>'*/sales_order/view'),
                            'field'   => 'order_id'
                        )
                    ),
                    'filter'    => false,
                    'sortable'  => false,
                    'index'     => 'stores',
                    'is_system' => true,
            ));
        }
        $this->addRssList('rss/order/new', Mage::helper('sales')->__('New Order RSS'));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));

        return Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
    }

}
