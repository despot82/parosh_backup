<?php

/**
 * @category   FiloBlu
 * @package    FiloBlu_AdminOrderGrid
 * @author     Francesco Zoccarato <francesco@filoblu.com>
 */
class FiloBlu_AdminOrderGrid_Block_Adminhtml_Sales_Order_Renderer_Coupon extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function __construct() {
        parent::_construct();
    }

    public function render(Varien_Object $row) {

        $coupon = '';
        try {
            $order = Mage::getModel('sales/order')->load($row->getEntityId());
            if ($order) {
                $coupon = $order->getData('coupon_code');
            }
        } catch (Exception $e) {
            
        }
        return $coupon;
    }

}
