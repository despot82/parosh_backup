<?php

/**
 * @category   FiloBlu
 * @package    FiloBlu_AdminProductGrid
 * @author     Francesco Zoccarato <francesco@filoblu.com>
 */
class FiloBlu_AdminProductGrid_Block_Adminhtml_Catalog_Product_Renderer_Image extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $product = Mage::getModel('catalog/product')->load($row->getEntityId());

        $mediaGalleryCount = count($product->getMediaGalleryImages());

        $imgHtml = '';
        if ($product) {
            try {
                $image = Mage::helper('catalog/image')->init($product, $this->getColumn()->getIndex())->resize(100)->constrainOnly(true)->keepAspectRatio(true)->keepFrame(false)->keepTransparency(true)->__toString();
                if ($image) {
                    $image = str_replace('/adminhtml/', '/frontend/', $image);
                    $imgHtml = '<span style="color:transparent;font-size:0">' . $mediaGalleryCount . '</span><img style="display:inline;float:left;" src="' . $this->escapeHtml($image) . '" width="100px" /> ';
                }
            } catch (Exception $e) {
            }
        }
        return $imgHtml . '<span>(' . $mediaGalleryCount . ')</span>';
    }

}
