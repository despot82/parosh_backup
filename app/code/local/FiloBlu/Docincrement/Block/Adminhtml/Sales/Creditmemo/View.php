<?php
class FiloBlu_Docincrement_Block_Adminhtml_Sales_Creditmemo_View extends Mage_Adminhtml_Block_Sales_Order_Creditmemo_View
{
    public function getHeaderText()
    {
        if ($this->getCreditmemo()->getEmailSent()) {
            $emailSent = Mage::helper('sales')->__('the credit memo email was sent');
        }
        else {
            $emailSent = Mage::helper('sales')->__('the credit memo email is not sent');
        }
        return Mage::helper('sales')->__('Credit Memo #%1$s | %3$s | %2$s (%4$s)', $this->getCreditmemo()->getDocIncrementId(), $this->formatDate($this->getCreditmemo()->getCreatedAtDate(), 'medium', true), $this->getCreditmemo()->getStateName(), $emailSent);
    }
}
?>