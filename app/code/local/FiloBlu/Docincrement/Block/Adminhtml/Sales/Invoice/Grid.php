<?php

class FiloBlu_Docincrement_Block_Adminhtml_Sales_Invoice_Grid extends Mage_Adminhtml_Block_Sales_Invoice_Grid
{

    public function __construct() {
        parent::__construct();
        $this->setDefaultLimit(30);
    }

    public function getAllWebsiteIds() {
        $a = array();
        foreach (Mage::app()->getWebsites() as $ws) {
            $a[$ws->getWebsiteId()] = $ws->getName();
        }
        //Mage::log(Mage::app()->getWebsites()->getIds());
        return $a;
    }


    protected function _prepareColumns()
    {
        $this->addColumn('doc_increment_id', array(
            'header'    => Mage::helper('sales')->__('Invoice #'),
            'index'     => 'doc_increment_id',
            'type'      => 'text',
            'getter'   => array($this, 'getDocIncrementId'),
            'filter_condition_callback' => array($this, '_filterDocIncrementId'),
        ));

        $this->addColumn('created_at', array(
            'header'    => Mage::helper('sales')->__('Invoice Date'),
            'index'     => 'created_at',
            'type'      => 'datetime',
            'filter_index'=>'main_table.created_at',
        ));

        $this->addColumn('order_increment_id', array(
            'header'    => Mage::helper('sales')->__('Order #'),
            'index'     => 'order_increment_id',
            'type'      => 'text',
            'filter_index'=>'main_table.order_increment_id',
        ));

        $this->addColumn('order_created_at', array(
            'header'    => Mage::helper('sales')->__('Order Date'),
            'index'     => 'order_created_at',
            'type'      => 'datetime',
            'filter_index'=>'main_table.order_created_at',
        ));

        $this->addColumn('billing_name', array(
            'header' => Mage::helper('sales')->__('Bill to Name'),
            'index' => 'billing_name',
            'filter_index'=>'main_table.billing_name',
        ));

        $this->addColumn('state', array(
            'header'    => Mage::helper('sales')->__('Status'),
            'index'     => 'state',
            'type'      => 'options',
            'options'   => Mage::getModel('sales/order_invoice')->getStates(),
            'filter_index'=>'main_table.state',
        ));

        $this->addColumn('grand_total', array(
            'header'    => Mage::helper('customer')->__('Amount'),
            'index'     => 'grand_total',
            'type'      => 'currency',
            'align'     => 'right',
            'currency'  => 'order_currency_code',
            'filter_index'=>'main_table.order_currency_code',
        ));

        $this->addColumn('action',
            array(
                'header'    => Mage::helper('sales')->__('Action'),
                'width'     => '50px',
                'type'      => 'action',
                'getter'     => 'getId',
                'actions'   => array(
                    array(
                        'caption' => Mage::helper('sales')->__('View'),
                        'url'     => array('base'=>'*/sales_invoice/view'),
                        'field'   => 'invoice_id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'is_system' => true
        ));

        $this->addColumn('website_id', array(
            'header' => Mage::helper('sales')->__('WID'),
            'index' => 'core_website.website_id',
            'type' => 'options',
            'width' => '80px',
            'options' => $this->getAllWebsiteIds(),
            'display_deleted' => true,
            'sortable' => false,
        ));

        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel XML'));

        return Mage_Adminhtml_Block_Widget_Grid::_prepareColumns();
    }


    protected function _prepareCollection() {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        $collection->getSelect()
                ->joinLeft('sales_flat_invoice', 'main_table.entity_id=sales_flat_invoice.entity_id', array( 'doc_increment_id' => 'doc_increment_id'))
                //->joinLeft(array('sales_order_data' => 'sales_flat_order'), 'main_table.entity_id=sales_order_data.entity_id', array('gw_id' => 'gw_id'))//
                //->joinLeft(array('sales_order_payment_data' => 'sales_flat_order_payment'), 'main_table.entity_id=sales_order_payment_data.parent_id', array('payer_country' => 'payer_country', 'payment_method' => 'method'))
                ->joinInner('core_store', 'main_table.store_id=core_store.store_id', array('store_view_name' => 'name'))
                ->joinInner('core_website', 'core_store.website_id=core_website.website_id', array('website_name' => 'name', 'website_id' => 'website_id'))
        ;



        $this->setCollection($collection);

        return Mage_Adminhtml_Block_Widget_Grid::_prepareCollection();
    }

     public function getDocIncrementId($row)
    {
        return $row->getDocIncrementId()?$row->getDocIncrementId():$row->getIncrementId();
    }

    protected function _filterMainTableColumn($collection, $column) {
        $select = $collection->getSelect();
        $field = $column->getIndex();
        $value = $column->getFilter()->getValue();

        $collection->addFieldToFilter('main_table.' . $field, array(
            'like' => "%" . $value . "%",
        ));
    }

    protected function _filterDocIncrementId($collection, $column) {
        $select = $collection->getSelect();
        //$field = $column->getIndex();
        $value = $column->getFilter()->getValue();

        $collection->addFieldToFilter(
            array('sales_flat_invoice.increment_id', 'sales_flat_invoice.doc_increment_id'),
            array(
                array('like'=>"%" . $value . "%"),
                array('like'=>"%" . $value . "%")
            )
        );
        //$collection->printLogQuery(false,true);

    }
}
