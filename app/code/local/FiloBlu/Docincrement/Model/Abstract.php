<?php


class FiloBlu_Docincrement_Model_Abstract extends Mage_Eav_Model_Entity_Abstract
{

    //ONLY FOR CEE STORES
    public function getDocNumber($storeId = null) {

        if($this->prefix!='extracee'){
            $websiteId = Mage::getModel('core/store')->load($storeId)->getWebsiteId();
            $storeId = Mage::getModel('core/website')->load($websiteId)
                        ->getDefaultGroup()
                        ->getDefaultStoreId();
        }
        else{
            $storeId = Mage::app()->getStore()->getStoreId();
        }
        
        return Mage::getSingleton('eav/config')
                        ->getEntityType('docnumber'.$this->prefix)
                        ->fetchNewIncrementId($storeId);
    }
}
