<?php


class FiloBlu_Docincrement_Model_Entity_Abstract extends Mage_Eav_Model_Entity_Abstract
{
    public function __construct()
    {
        $resource = Mage::getSingleton('core/resource');
        $this->setType('docnumber'.$this->prefix);
        $read = $resource->getConnection('sales_read');
        $write = $resource->getConnection('sales_write');
        $this->setConnection($read, $write);
    }
    
}
