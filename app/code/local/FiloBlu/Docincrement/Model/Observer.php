<?php

class FiloBlu_Docincrement_Model_Observer extends Varien_Object {

    const DOCNUMBER_ENABLED='docincrement/document_number/enabled';
    const DOCNUMBER_CODE='docnumberunified';


    public function sales_order_invoice_save_before($observer) {

        $item = $observer->getEvent()->getInvoice();
        $invoice = $item->load($item->getId());

        if(!$invoice->getData('increment_id')){
            if(Mage::getStoreConfig(self::DOCNUMBER_ENABLED)){
                  try {

                      $order = Mage::getModel("sales/order")->load($invoice->getOrderId());
                      $this->setDocIncrementId($invoice, self::DOCNUMBER_CODE);

                  } catch (Exception $e) {

                  }
              }
        }
        else if(!$invoice->getData('doc_increment_id')){
            $invoice->setData('doc_increment_id', $invoice->getData('increment_id'));
        }

    }


    public function sales_order_creditmemo_save_before($observer) {

        $item = $observer->getEvent()->getCreditmemo();
        $creditMemo = $item->load($item->getId());


        if(!$creditMemo->getData('increment_id')){

            if(Mage::getStoreConfig(self::DOCNUMBER_ENABLED)){
                try {

                    $order = Mage::getModel("sales/order")->load($creditMemo->getOrder()->getId());
                    $this->setDocIncrementId($creditMemo, self::DOCNUMBER_CODE);

                } catch (Exception $e) {

                }

            }
        }
        else if(!$creditMemo->getData('doc_increment_id')){
            $creditMemo->setData('doc_increment_id', $invoice->getData('increment_id'));
        }
        
    }




    private function setDocIncrementId(&$doc,$type) {

        if(!$doc->getData('doc_increment_id')){
            $order=$doc->getOrder();
            $doc->setData('doc_increment_id', Mage::getModel('docincrement/'.$type.'')->getDocNumber($order->getStoreId()));
        }
        else{
            $doc->setData('doc_increment_id', $doc->getData('increment_id'));
        }
    }




}
