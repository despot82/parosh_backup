<?php

$installer = $this;

$installer->startSetup();



$installer->addEntityType('docnumberunified',
  array(
    'entity_model'          =>'docincrement/docnumberunified',
    'attribute_model'       =>'',
    'table'                 =>'docincrement/docnumberunified',
    'increment_model'       =>'eav/entity_increment_numeric',
    'increment_per_store'   =>'0',
    'is_data_sharing'       =>'0',
    'data_sharing_key'      =>'default',
    'increment_pad_length'  =>'8'
));


$installer->endSetup(); 