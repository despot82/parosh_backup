<?php

$installer = $this;

$installer->startSetup();

$installer->getConnection()->addColumn($this->getTable('sales_flat_invoice'), 'doc_increment_id', "varchar(50) NULL");
$installer->getConnection()->addColumn($this->getTable('sales_flat_creditmemo'), 'doc_increment_id', "varchar(50) NULL");

$installer->endSetup(); 