<?php

class FiloBlu_Iris_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * 
     * @param type $sku
     * @return boolean return true if product already exists, false altrimenti
     */
    public function productExist($sku){
        $productId = Mage::getModel("catalog/product")->getIdBySku($sku);
        
        if($productId){
            return true;
        }
        return false;
    }
}