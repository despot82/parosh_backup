<?php

class OrderExportResult {
    /** @var String  */ 
    public $result_code;    
    /** @var OrderExport[]  */
    public $orders;   
}

class OrderExport {    
    /** @var OrderHeader  */ 
    public $header;
    /** @var OrderRow[]  */
    public $rows;        
}

class OrderHeader {
    /** @var String  */
    public $order_id;
    /** @var String  */
    public $order_date;
    /** @var String  */
    public $customer_id;
    /** @var String  */
    public $customer_email;
    /** @var String  */
    public $billing_firstname;
    /** @var String  */
    public $billing_lastname;
    /** @var String  */
    public $billing_street;
    /** @var String  */
    public $billing_co;
    /** @var String  */
    public $billing_zipcode;
    /** @var String  */
    public $billing_city;
    /** @var String  */
    public $billing_region;
    /** @var String  */
    public $billing_nation;
    /** @var String  */
    public $codice_fiscale;
    /** @var String  */
    public $piva;
    /** @var String  */
    public $billing_phone;
    /** @var String  */
    public $payment_method;
    /** @var String  */
    public $shipping_method;
    /** @var String  */
    public $shipping_firstname;
    /** @var String  */
    public $shipping_lastname;
    /** @var String  */
    public $shipping_street;
    /** @var String  */
    public $shipping_co;
    /** @var String  */
    public $shipping_zipcode;
    /** @var String  */
    public $shipping_city;
    /** @var String  */
    public $shipping_region;
    /** @var String  */
    public $shipping_nation;
    /** @var String  */
    public $shipping_phone;
    /** @var String  */
    public $currency_code;
    /** @var String  */
    public $shipping_cost;
    /** @var String  */
    public $order_total;
}

class OrderRow {
    /** @var String  */
    public $order_id;
    /** @var String  */
    public $sku;
    /** @var String  */
    public $qty;
    /** @var String  */
    public $price;            
}

class FiloBlu_Iris_Helper_Order {
    /* CAMPI DA ESPORTARE

    state, status, coupon_code, shipping_description, store_id, customer, base_discount_amount, base_grand_total, base_shipping_amount, base_shipping_tax_amount, base_subtotal, base_tax_amount, base_to_global_rate, base_to_order_rate, discount_amount, grand_total, shipping_amount, shipping_tax_amount, subtotal, tax_amount, total_qty_ordered, base_subtotal_incl_tax, subtotal_incl_tax, increment_id, base_currency_code, global_currency_code, order_currency_code, remote_ip, shipping_method, created_at, total_item_count, shipping_incl_tax, base_shipping_incl_tax

     */
    
    public function getOrderExportObject(){
        $obj = new OrderExportResult();
        return $obj;
    }

    /**
     * Completes the Shipment, followed by completing the Order life-cycle
     * It is assumed that the Invoice has already been generated
     * and the amount has been captured.
     */
    public function completeShipment($orderIncrementId, $shipmentTrackingNumber, $customerEmailComments, $shipmentCarrierCode, $shipmentCarrierTitle, $send_email = false) {


        $order = Mage::getModel('sales/order')
                ->loadByIncrementId($orderIncrementId);

        if (!$order->getId()) {
            Mage::throwException("Order does not exist, for the Shipment process to complete");
        }

        if ($order->canShip()) {
            try {
                $shipment = Mage::getModel('sales/service_order', $order)
                        ->prepareShipment($this->__getItemQtys($order));

                /**
                 * Carrier Codes can be like "ups" / "fedex" / "custom",
                 * but they need to be active from the System Configuration area.
                 * These variables can be provided custom-value, but it is always
                 * suggested to use Order values
                 */
                //$shipmentCarrierCode = 'SPECIFIC_CARRIER_CODE';
                //$shipmentCarrierTitle = 'SPECIFIC_CARRIER_TITLE';

                $arrTracking = array(
                    'carrier_code' => isset($shipmentCarrierCode) ? $shipmentCarrierCode : $order->getShippingCarrier()->getCarrierCode(),
                    'title' => isset($shipmentCarrierTitle) ? $shipmentCarrierTitle : $order->getShippingCarrier()->getConfigData('title'),
                    'number' => $shipmentTrackingNumber,
                );

                $track = Mage::getModel('sales/order_shipment_track')->addData($arrTracking);
                $shipment->addTrack($track);

                // Register Shipment
                $shipment->register();

                // Save the Shipment
                $this->__saveShipment($shipment, $order, $customerEmailComments, $send_email);

                // Finally, Save the Order
                $this->__saveOrder($order);
            } catch (Exception $e) {
                throw $e;
            }
        }else{
            Mage::throwException("Cannot ship order ".$orderIncrementId);            
        }
    }
    
    /**
     * Get the Quantities shipped for the Order, based on an item-level
     * This method can also be modified, to have the Partial Shipment functionality in place
     *
     * @param $order Mage_Sales_Model_Order
     * @return array
     */
    private function __getItemQtys(Mage_Sales_Model_Order $order) {
        $qty = array();

        foreach ($order->getAllItems() as $_eachItem) {
            if ($_eachItem->getParentItemId()) {
                $qty[$_eachItem->getParentItemId()] = $_eachItem->getQtyOrdered();
            } else {
                $qty[$_eachItem->getId()] = $_eachItem->getQtyOrdered();
            }
        }

        return $qty;
    }

    /**
     * Saves the Shipment changes in the Order
     *
     * @param $shipment Mage_Sales_Model_Order_Shipment
     * @param $order Mage_Sales_Model_Order
     * @param $customerEmailComments string
     */
    private function __saveShipment(Mage_Sales_Model_Order_Shipment $shipment, Mage_Sales_Model_Order $order, $customerEmailComments = '', $send_email) {
        $shipment->getOrder()->setIsInProcess(true);
        $transactionSave = Mage::getModel('core/resource_transaction')
                ->addObject($shipment)
                ->addObject($order)
                ->save();

        $emailSentStatus = $shipment->getData('email_sent');
        if ($send_email && !$emailSentStatus) {
            $shipment->sendEmail(true, $customerEmailComments);
            $shipment->setEmailSent(true);
        }

        return $this;
    }

    /**
     * Saves the Order, to complete the full life-cycle of the Order
     * Order status will now show as Complete
     *
     * @param $order Mage_Sales_Model_Order
     */
    private function __saveOrder(Mage_Sales_Model_Order $order) {
        $order->setData('state', Mage_Sales_Model_Order::STATE_COMPLETE);
        $order->setData('status', Mage_Sales_Model_Order::STATE_COMPLETE);

        $order->save();

        return $this;
    }
    
    
    /**
     * Cambia lo stato dell'ordine
     * @param type $orderId
     * @param type $status
     */
    public function updateOrderStatus($orderId, $status) {
        $order = Mage::getModel("sales/order")->loadByIncrementId($orderId);
        
        if ($order->getId()) {
            $order->setData('status', $status);
            $order->save();
            Mage::helper('iris/log')->log('Order update: ' . $order->getId() . " with status: " . $order->getStatus());
            return true;
        } else {
            Mage::helper('iris/log')->log('Unable to update order: ' . $orderId . " with status: " . $status);
            return false;
            
        }
    }

    /**
     * Esporta l'ordine in formato array
     * @param type $orderId è l'entityId dell'ordine
     */
    public function exportSingleOrderData($orderId) {
        $order = Mage::getModel("sales/order")->load($orderId);
        if ($order->getId()) {

            //testata
            $orderHeader = $this->__prepareOrder($order);


            //righe
            $row = 1;
            $orderRows = array();
            foreach ($order->getAllItems() as $item) {
                if ($item->getProductType() != Mage_Catalog_Model_Product_Type::TYPE_SIMPLE) {
                    continue;
                }


                $orderRows[] = $this->__prepareItem($item, $row, $order);
                $row++;
            }

            return array("header" => $orderHeader, "rows" => $orderRows);
        }
        return null;
    }

    /**
     * Esporta l'ordine in formato array
     * @param type $orderId è l'entityId dell'ordine
     */
    public function exportSingleOrderDataObject($orderId) {
        $order = Mage::getModel("sales/order")->load($orderId);
        if ($order->getId()) {
            $orderExport = new OrderExport();

            //testata
            $orderHeader = $this->__prepareOrderObject($order);
            $orderExport->header = $orderHeader;
            


            //righe
            $row = 1;
            foreach ($order->getAllItems() as $item) {
                if ($item->getProductType() != Mage_Catalog_Model_Product_Type::TYPE_SIMPLE) {
                    continue;
                }


                $orderExport->rows[] = $this->__prepareItemObject($item, $row, $order);
                $row++;
            }
            

            return $orderExport;
        }
        return null;
    }    
    
    private function __prepareOrderObject($order) {
        $orderObject = new OrderHeader();
        $incrementId = $order->getIncrementId();

        $clientOrderDate = $evasione = $orderDate = $order->getCreatedAt();

        if ($order->getCustomerIsGuest() || !$order->getCustomerId())
            $customerId = $incrementId;
        else
            $customerId = $order->getCustomerId();
        

        $billingAddress = $order->getBillingAddress();
        $shippingAddress = $order->getShippingAddress();

        if ($order->getCustomerIsGuest() || !$order->getCustomerId()) {
            $billingId = $incrementId;
        } else {
            if ($billingAddress->getId() && $billingAddress->getCustomerAddressId()) {
                $billingId = $billingAddress->getCustomerAddressId();
            } else {
                $billingId = $incrementId;
            }
        }
        $firstname = $billingAddress->getFirstname();
        $lastname = $billingAddress->getLastname();
        $presso = $billingAddress->getCompany();
        $street = implode(" ", $billingAddress->getStreet());
        $zip = $billingAddress->getPostcode();

        $loca = $billingAddress->getCity();

        $country = Mage::getModel('directory/country')->loadByCode($billingAddress->getCountryId());

        $provincia = $billingAddress->getRegion();

        if ($billingAddress->getRegionId() && $billingAddress->getCountryId() == 'IT')
            $provincia = Mage::getModel('directory/region')->load($billingAddress->getRegionId())->getCode();



        $nazione = $billingAddress->getCountry();

        $pIva = $order->getCustomerTaxvat();
        $cf = $billingAddress->getVatId();
        
        $tel = $billingAddress->getTelephone();
        $email = $order->getCustomerEmail();

        $pressoShipping = $shippingAddress->getCompany();
        if ($pressoShipping)
            $pressoShipping = $pressoShipping;
        else
            $pressoShipping = '';

        if ($order->getCustomerIsGuest() || !$order->getCustomerId()) {
            $idShipping = $incrementId;
        } else {
            if ($shippingAddress->getId() && $shippingAddress->getCustomerAddressId()) {
                $idShipping = $shippingAddress->getCustomerAddressId();
            } else {
                $idShipping = $incrementId;
            }
        }

        $firstnameShipping = $shippingAddress->getFirstname();
        $lastnameShipping = $shippingAddress->getLastname();
        $streetShipping = implode(" ", $shippingAddress->getStreet());
        $zipShipping = $shippingAddress->getPostcode();
        $locaShipping = $shippingAddress->getCity();
        $countrySh = Mage::getModel('directory/country')->loadByCode($shippingAddress->getCountryId());
        $provinciaShipping = $shippingAddress->getRegion();
        if ($shippingAddress->getRegionId() && $shippingAddress->getCountryId() == 'IT')
            $provinciaShipping = Mage::getModel('directory/region')->load($shippingAddress->getRegionId())->getCode();

        $nazioneShipping = $shippingAddress->getCountry();
        $pIvaShipping = $cfShipping = $order->getCustomerTaxvat();
        $telShipping = $shippingAddress->getTelephone();
        $emailShipping = $order->getCustomerEmail();
        $shippingCost = $order->getShippingInclTax();
        $shippingMethod = $order->getShippingDescription();
        $discountAmount = $order->getDiscountAmount();
        $couponCode = $order->getCouponCode();
        $vettore = ($shippingAddress->getCountryId() == 'IT') ? 'TNT' : 'UPS';
        $note = $confezioneRegalo = $bigliettoAuguri = $testoAuguri = '';

        /*
         * Confezione regalo
         */
        if ($order->getGwId())
            $confezioneRegalo = 1;

        $giftCost = $order->getGwPrice() + $order->getGwTaxAmount();
        if ($order->getGiftMessageId()) {
            $giftMessage = str_replace("\n", " ", Mage::getModel('giftmessage/message')->load($order->getGiftMessageId())->getMessage());
            $giftMessage = str_replace("\r\n", " ", $giftMessage);
        } else {
            $giftMessage = '';
        }

        $orderTotal = $order->getGrandTotal();

        $paymentMethod = $order->getPayment()->getMethodInstance()->getTitle();

        $valuta = $order->getOrderCurrencyCode();

        $orderObject->order_id = $incrementId;
        $orderObject->order_date = $clientOrderDate;
        $orderObject->customer_id = $customerId;
        $orderObject->customer_email = $email;
        $orderObject->billing_firstname = $firstname;
        $orderObject->billing_lastname = $lastname;
        $orderObject->billing_street = $street;
        $orderObject->billing_co = $presso;
        $orderObject->billing_zipcode = $zip;
        $orderObject->billing_city = $loca;
        $orderObject->billing_region = $provincia;
        $orderObject->billing_nation = $nazione;
        $orderObject->codice_fiscale = $cf;
        $orderObject->piva = $pIva;
        $orderObject->billing_phone = $tel;
        $orderObject->payment_method = $paymentMethod;
        $orderObject->shipping_method = $shippingMethod;
        $orderObject->shipping_firstname = $firstnameShipping;
        $orderObject->shipping_lastname = $lastnameShipping;
        $orderObject->shipping_street = $streetShipping;
        $orderObject->shipping_co = $pressoShipping;
        $orderObject->shipping_zipcode = $zipShipping;
        $orderObject->shipping_city = $locaShipping;
        $orderObject->shipping_region = $provinciaShipping;
        $orderObject->shipping_nation = $nazioneShipping;
        $orderObject->shipping_phone = $telShipping;
        $orderObject->currency_code = $valuta;
        $orderObject->shipping_cost = number_format($shippingCost, 2);
        $orderObject->order_total = number_format($orderTotal, 2);

        return $orderObject;
    }    
    
    private function __prepareItemObject($item, $row, $order) {

        $parent = $item;

        if ($item->getParentItemId())
            $parent = $item->getParentItem();

        $qty = $this->__formatQtyOrdered($parent->getQtyOrdered());
        $product = Mage::getModel("catalog/product")->load($item->getProductId());

        $total = (float) ($parent->getRowTotalInclTax() / $qty);

        $orderRow = new OrderRow();

        $orderRow->order_id = $order->getIncrementId();
        $orderRow->sku = $product->getSku();
        $orderRow->qty = $qty;
        $orderRow->price = number_format($total, 2);


        return $orderRow;
    }    
    
    private function __prepareOrder($order) {

        $incrementId = $order->getIncrementId();

        $clientOrderDate = $evasione = $orderDate = $order->getCreatedAt();

        if ($order->getCustomerIsGuest() || !$order->getCustomerId())
            $customerId = $incrementId;
        else
            $customerId = $order->getCustomerId();
        

        $billingAddress = $order->getBillingAddress();
        $shippingAddress = $order->getShippingAddress();

        if ($order->getCustomerIsGuest() || !$order->getCustomerId()) {
            $billingId = $incrementId;
        } else {
            if ($billingAddress->getId() && $billingAddress->getCustomerAddressId()) {
                $billingId = $billingAddress->getCustomerAddressId();
            } else {
                $billingId = $incrementId;
            }
        }
        $firstname = $billingAddress->getFirstname();
        $lastname = $billingAddress->getLastname();
        $presso = $billingAddress->getCompany();
        $street = implode(" ", $billingAddress->getStreet());
        $zip = $billingAddress->getPostcode();

        $loca = $billingAddress->getCity();

        $country = Mage::getModel('directory/country')->loadByCode($billingAddress->getCountryId());

        $provincia = $billingAddress->getRegion();

        if ($billingAddress->getRegionId() && $billingAddress->getCountryId() == 'IT')
            $provincia = Mage::getModel('directory/region')->load($billingAddress->getRegionId())->getCode();



        $nazione = $billingAddress->getCountry();

        $pIva = $order->getCustomerTaxvat();
        $cf = $billingAddress->getVatId();
        
        $tel = $billingAddress->getTelephone();
        $email = $order->getCustomerEmail();

        $pressoShipping = $shippingAddress->getCompany();
        if ($pressoShipping)
            $pressoShipping = $pressoShipping;
        else
            $pressoShipping = '';

        if ($order->getCustomerIsGuest() || !$order->getCustomerId()) {
            $idShipping = $incrementId;
        } else {
            if ($shippingAddress->getId() && $shippingAddress->getCustomerAddressId()) {
                $idShipping = $shippingAddress->getCustomerAddressId();
            } else {
                $idShipping = $incrementId;
            }
        }

        $firstnameShipping = $shippingAddress->getFirstname();
        $lastnameShipping = $shippingAddress->getLastname();
        $streetShipping = implode(" ", $shippingAddress->getStreet());
        $zipShipping = $shippingAddress->getPostcode();
        $locaShipping = $shippingAddress->getCity();
        $countrySh = Mage::getModel('directory/country')->loadByCode($shippingAddress->getCountryId());
        $provinciaShipping = $shippingAddress->getRegion();
        if ($shippingAddress->getRegionId() && $shippingAddress->getCountryId() == 'IT')
            $provinciaShipping = Mage::getModel('directory/region')->load($shippingAddress->getRegionId())->getCode();

        $nazioneShipping = $shippingAddress->getCountry();
        $pIvaShipping = $cfShipping = $order->getCustomerTaxvat();
        $telShipping = $shippingAddress->getTelephone();
        $emailShipping = $order->getCustomerEmail();
        $shippingCost = $order->getShippingInclTax();
        $shippingMethod = $order->getShippingDescription();
        $discountAmount = $order->getDiscountAmount();
        $couponCode = $order->getCouponCode();
        $vettore = ($shippingAddress->getCountryId() == 'IT') ? 'TNT' : 'UPS';
        $note = $confezioneRegalo = $bigliettoAuguri = $testoAuguri = '';

        /*
         * Confezione regalo
         */
        if ($order->getGwId())
            $confezioneRegalo = 1;

        $giftCost = $order->getGwPrice() + $order->getGwTaxAmount();
        if ($order->getGiftMessageId()) {
            $giftMessage = str_replace("\n", " ", Mage::getModel('giftmessage/message')->load($order->getGiftMessageId())->getMessage());
            $giftMessage = str_replace("\r\n", " ", $giftMessage);
        } else {
            $giftMessage = '';
        }

        $orderTotal = $order->getGrandTotal();

        $paymentMethod = $order->getPayment()->getMethodInstance()->getTitle();

        $valuta = $order->getOrderCurrencyCode();

        $dataOrder = array(
            'id' => $incrementId,
            'order_date' => $clientOrderDate,
            'customer_id' => $customerId,
            'customer_email' => $email,
            'billing_firstname' => $firstname,
            'billing_lastname' => $lastname,
            'billing_street' => $street,
            'billing_co' => $presso,
            'billing_zipcode' => $zip,
            'billing_city' => $loca,
            'billing_region' => $provincia,
            'billing_nation' => $nazione,
            'codice_fiscale' => $cf,
            'piva' => $pIva,
            'billing_phone' => $tel,
            'payment_method' => $paymentMethod,
            'shipping_method' => $shippingMethod,
            'shipping_firstname' => $firstnameShipping,
            'shipping_lastname' => $lastnameShipping,
            'shipping_street' => $streetShipping,
            'shipping_co' => $pressoShipping,
            'shipping_zipcode' => $zipShipping,
            'shipping_city' => $locaShipping,
            'shipping_region' => $provinciaShipping,
            'shipping_nation' => $nazioneShipping,
            'shipping_phone' => $telShipping,
            'currency_code' => $valuta,
            'shipping_cost' => number_format($shippingCost, 2),
            'order_total' => number_format($orderTotal, 2)
        );

        return $dataOrder;
    }

    private function __prepareItem($item, $row, $order) {

        $parent = $item;

        if ($item->getParentItemId())
            $parent = $item->getParentItem();

        $qty = $this->__formatQtyOrdered($parent->getQtyOrdered());
        $product = Mage::getModel("catalog/product")->load($item->getProductId());

        $total = (float) ($parent->getRowTotalInclTax() / $qty);

        $itemData = array(
            'order_id' => $order->getIncrementId(),
            'sku' => $product->getSku(),
            'qty' => $qty,
            'price' => number_format($total, 2),
        );

        return $itemData;
    }

    private function __formatQtyOrdered($qty) {
        list($int, $dec) = explode(".", $qty);

        $int = str_replace(",", "", $int);
        $dec = $this->__formatLenght($dec, 3);

        return $int . '.' . $dec;
    }

    private function __formatLenght($string, $num) {

        $string = str_replace(array(";", "''"), " ", $string);

        if (strlen(utf8_decode($string)) >= $num)
            return substr($string, 0, $num);

        for ($i = strlen(utf8_decode($string)); $i < $num; $i++)
            $string .= " ";


        return utf8_decode($string);
    }
        
}

?>
