<?php

class FiloBlu_Iris_Helper_Seo extends Mage_Core_Helper_Abstract {

    const META_TITLE_RULE = 'iris_seo/settings/metatitle';
    const META_KEYWORDS_RULE = 'iris_seo/settings/metakeywords';
    const META_DESCRIPTION_RULE = 'iris_seo/settings/metadescription';
    const META_SUBSTITUTION = 'iris_seo/settings/overridemeta';
    const META_COLLECTION = 'iris_seo/settings/updateproducts';


    /**
     * Log in specific file
     * @param type $message
     */
    public function log($message, $level = null) {
        Mage::log($message, $level, "iris.log");
    }
    
    public function generateMeta($storeId){
        $meta_collection = Mage::getStoreConfig(self::META_COLLECTION);
        if($meta_collection == "1"){
            $collection = Mage::getModel("catalog/product")->getCollection();
            foreach ($collection as $product) {
                $this->generateMetaForProductId($product->getId(),$storeId);
            }
        }
        
    }

    public function generateMetaForProductId($productId, $storeId) {
        $product = Mage::getModel("catalog/product")->setStoreId($storeId)->load($productId);
        
        $metaArray = array(
            "meta_title" => $product->getMetaTitle(),
            "meta_keyword" => $product->getMetaKeyword(),
            "meta_description" => $product->getMetaDescription(),
        );
        
        $metaArrayGenerated = $this->substitutionMethod($product,$storeId);
        //controllo se i meta sono da sostituire
        $metaSubstitution = Mage::getStoreConfig(self::META_SUBSTITUTION);
        
        $update = false;
        if($metaSubstitution == 1){
            $update = true;
            $metaArray["meta_title"] = $metaArrayGenerated["meta_title"];
            $metaArray["meta_keyword"] = $metaArrayGenerated["meta_keyword"];
            $metaArray["meta_description"] = $metaArrayGenerated["meta_description"];            
        }else{
            if(trim($metaArray["meta_title"]) == ""){
                $update = true;
                $metaArray["meta_title"] = $metaArrayGenerated["meta_title"];
            }
            if(trim($metaArray["meta_keyword"]) == ""){            
                $update = true;
                $metaArray["meta_keyword"] = $metaArrayGenerated["meta_keyword"];
            }
            if(trim($metaArray["meta_description"]) == ""){            
                $update = true;
                $metaArray["meta_description"] = $metaArrayGenerated["meta_description"];                        
            }
        }
        
        if($update){
            Mage::helper("iris/log")->log("Aggiornamento meta per prodotto ".$productId);
            Mage::getSingleton('catalog/product_action')->updateAttributes(array($productId), $metaArray, $storeId);            
        }
    }

    /* Sostituisce le seguenti variabili con i valori corretti dei prodotti
      {{PRODUCT_NAME}} = nome prodotto
      {{PRODUCT_CATEGORY}} = prima categoria del prodotto
      {{PRODUCT_CATEGORIES}} = categorie prodotto separate da virgola
      {{PRODUCT_ATTRIBUTE}} = valore di un attributo di un prodotto. Un esempio può essre {{PRODUCT_DESCRIPTION}} oppure {{PRODUCT_COLOR}}
     */

    public function substitutionMethod($productModel,$storeId) {
        $meta_title_rule = $this->getMetaTitleRule();
        $meta_keyword_rule = $this->getMetaKeyworsRule();
        $meta_description_rule = $this->getMetaDescriptionRule();

        $metaRulesArray = array(
            "meta_title" => $meta_title_rule,
            "meta_keyword" => $meta_keyword_rule,
            "meta_description" => $meta_description_rule
        );

        $patternArray = array(
            "{{PRODUCT_NAME}}",
            "{{PRODUCT_CATEGORY}}",
            "{{PRODUCT_CATEGORIES}}",
            "{{PRODUCT_ATTRIBUTE}}"
        );

        //ciclo le regole
        foreach ($metaRulesArray as $key => $rule) {
            //per ogni regola applico un replace dei pattern
            foreach ($patternArray as $patter) {
                //Mage::helper("iris/log")->log("Searching for ".$patter);
                switch ($patter) {
                    case "{{PRODUCT_NAME}}":
                        $rule = str_replace("{{PRODUCT_NAME}}", $this->getAttributeValueText($productModel, "name",$storeId), $rule);
                        break;
                    case "{{PRODUCT_CATEGORY}}":
                        $categoryIds = $productModel->getCategoryIds();
                        $categoryName = "";
                        $count = 0;
                        foreach ($categoryIds as $catId) {
                            $category = Mage::getModel("catalog/category")->setStoreId($storeId)->load($catId);
                            $categoryName = $categoryName . ", " . $category->getName();
                            $count++;
                            if ($count == 1) {
                                break;
                            }
                        }
                        $rule = str_replace("{{PRODUCT_CATEGORY}}", trim($categoryName, ","), $rule);
                        break;
                    case "{{PRODUCT_CATEGORIES}}":
                        $categoryIds = $productModel->getCategoryIds();
                        $categoryName = "";
                        $count = 0;
                        foreach ($categoryIds as $catId) {
                            $category = Mage::getModel("catalog/category")->load($catId);
                            $categoryName = $categoryName . ", " . $category->getName();
                            $count++;
                        }
                        $rule = str_replace("{{PRODUCT_CATEGORIES}}", trim($categoryName, ","), $rule);
                        break;
                    default:
                        //{{PRODUCT_ATTRIBUTE}}
                        //1) cerco se presente {{PRODUCT_*}}
                        
                        $match = false;
                        $pattern = '/({{PRODUCT_[A-Z_]+}})/';
                        $match = preg_match($pattern, $rule);                        
                        while($match == true){



                            //2) se lo trovo cerco di prendere l'attribute code
                            if ($match) {
                                $matches = array();
                                $subject = "ciao {{PRODUCT_IMAGE}} {{PRODUCT_ATT}} ";
                                $pattern = '/({{PRODUCT_[A-Z_]+}})/';
                                preg_match($pattern, $rule, $matches, PREG_OFFSET_CAPTURE);

                                $matchedString = $matches[0][0];
                                //la ripulisco
                                $matchedString = str_replace("{{", "", $matchedString);
                                $matchedString = str_replace("}}", "", $matchedString);
                                $matchedString = str_replace("PRODUCT_", "", $matchedString);
                                $matchedString = strtolower(trim($matchedString));
                                $rule = str_replace($matches[0][0], $this->getAttributeValueText($productModel, $matchedString, $storeId), $rule);
                                $metaRulesArray[$key] = $rule;
                            }
                            
                            $match = false;
                            $pattern = '/({{PRODUCT_[A-Z_]+}})/';
                            $match = preg_match($pattern, $rule);                                                    
                        }
                        break;
                }
                $metaRulesArray[$key] = $rule;
            }
        }
        
        return $metaRulesArray;

        
    }

    private function getAttributeValueText($productModel, $attributeCode, $storeId) {
        try {

            $attribute = $productModel->getResource()->getAttribute($attributeCode);
            if(!$attribute){
                throw new Exception("attributo non trovato");
            }
            $attributeFrontendInput = $attribute->getFrontendInput();

            switch ($attributeFrontendInput) {
                case "text":
                    return trim(Mage::helper("iris")->__($productModel->getResource()->getAttribute($attributeCode)->setStoreId($storeId)->getFrontend()->getValue($productModel)));
                    //return trim($productModel->getData($attributeCode));
                    break;
                case "textarea":
                    return trim(Mage::helper("iris")->__($productModel->getResource()->getAttribute($attributeCode)->setStoreId($storeId)->getFrontend()->getValue($productModel)));
                    break;
                case "select":
                    return trim(Mage::helper("iris")->__($productModel->getResource()->getAttribute($attributeCode)->setStoreId($storeId)->getFrontend()->getValue($productModel)));
                    break;
                case "multiselect":
                    $retString = Mage::helper("iris")->__($productModel->getResource()->getAttribute($attributeCode)->setStoreId($storeId)->getFrontend()->getValue($productModel));                    
                    return $retString;
                    break;
                default:
                    return "";
                    break;
            }
        } catch (Exception $e) {
            $this->log("Errore nel prendere il valore dell'attributo:".$attributeCode."|");
            $this->log($e->getMessage());
            return "";
        }
    }

    public function getMetaTitleRule() {
        return Mage::getStoreConfig(self::META_TITLE_RULE);
    }

    public function getMetaKeyworsRule() {
        return Mage::getStoreConfig(self::META_KEYWORDS_RULE);
    }

    public function getMetaDescriptionRule() {
        return Mage::getStoreConfig(self::META_DESCRIPTION_RULE);
    }

}

?>
