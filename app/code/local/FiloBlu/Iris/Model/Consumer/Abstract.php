<?php

abstract class FiloBlu_Iris_Model_Consumer_Abstract implements FiloBlu_Iris_Model_Consumer_Interface{
    abstract function consumeRows();
    
    abstract function consumeSimpleProductRow($rowId);
}
?>
