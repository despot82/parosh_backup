<?php

class FiloBlu_Iris_Model_Consumer_Inventory_Csv_Default extends FiloBlu_Iris_Model_Consumer_Inventory_Default {
    

    function __construct() {       
    }
    
    public function consumeRows() {
        //prendo tutte le righe ancora da processare ordinate in modo crescente
        $collection = Mage::getModel("iris/action_inventory")->getCollection()
                ->addFieldToFilter('status', FiloBlu_Iris_Model_Dal_Inventory_Default::ACTION_STATUS_TO_BE_PROCESSED)
                ->setOrder("id", Varien_Data_Collection::SORT_ORDER_ASC);

        //Consume le righe di stock
        Mage::helper("iris/log")->log("inizio aggiornamento inventory");
        foreach ($collection as $action) {
            if ($action->getActionType() == FiloBlu_Iris_Model_Dal_Order_Default::ACTION_TYPE_INSERT) {
                $this->consumeInventory($action->getId());
                $action->setHasBeenProcessed();
            } else {
                Mage::helper("iris/log")->log("Azione per inventory non implementata");
            }
        }   
        Mage::helper("iris/log")->log("fine aggiornamento inventory");
        

    }  
    
    public function consumeInventory($rowId) {
        //prendo il rowId e formatto i valori da passare all'helper che crea/aggiorna stocks
        $actionModel = Mage::getModel("iris/action_inventory")->load($rowId);
        if ($actionModel->getId()) {
            //controllo il tipo di azione
            switch ($actionModel->getActionType()) {
                case FiloBlu_Iris_Model_Dal_Inventory_Default::ACTION_TYPE_INSERT:
                    //aggiorno lo stock
                    $productId = Mage::getModel("catalog/product")->getIdBySku($actionModel->getSku());
                    Mage::helper("iris/inventory")->updateProductInventory($productId, $actionModel->getQty());                    
                    $actionModel->setHasBeenProcessed();
                    break;
                case FiloBlu_Iris_Model_Dal_Inventory_Default::ACTION_TYPE_UPDATE:
                    Mage::helper("iris/log")->log("azione non implementata");
                    break;
                default:
                    break;
            }
        }
    }

}

?>
