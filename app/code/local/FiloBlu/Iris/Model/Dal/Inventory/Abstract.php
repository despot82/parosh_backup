<?php

abstract class FiloBlu_Iris_Model_Dal_Inventory_Abstract implements FiloBlu_Iris_Model_Dal_Inventory_Interface{
        
    abstract function insertAction(FiloBlu_Iris_Model_Data_Inventory_Default $object);
    
    abstract function updateAction(FiloBlu_Iris_Model_Data_Inventory_Default $object);
    
    abstract function deleteAction(FiloBlu_Iris_Model_Data_Inventory_Default $object);
    
    abstract function cleanAction();


}
?>
