<?php

interface FiloBlu_Iris_Model_Dal_Inventory_Interface {
    
    public function insertAction(FiloBlu_Iris_Model_Data_Inventory_Default $object);
    
    public function updateAction(FiloBlu_Iris_Model_Data_Inventory_Default $object);
    
    public function deleteAction(FiloBlu_Iris_Model_Data_Inventory_Default $object);
    
    public function cleanAction();
        
    
}
?>
