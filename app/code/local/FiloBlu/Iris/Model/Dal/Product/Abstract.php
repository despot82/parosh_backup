<?php

abstract class FiloBlu_Iris_Model_Dal_Product_Abstract implements FiloBlu_Iris_Model_Dal_Product_Interface{
        
    abstract function insertAction(FiloBlu_Iris_Model_Data_Product_Default $object);
    
    abstract function updateAction(FiloBlu_Iris_Model_Data_Product_Default $object);
    
    abstract function deleteAction(FiloBlu_Iris_Model_Data_Product_Default $object);
    
    abstract function cleanAction();


}
?>
