<?php

abstract class FiloBlu_Iris_Model_Parser_Inventory_Abstract implements FiloBlu_Iris_Model_Parser_Inventory_Interface{
    
    abstract public function readData();
    
    public function utility(){
        return "data utility";
    }    
}

