<?php

class FiloBlu_Iris_Model_Parser_Inventory_Csv_Default extends FiloBlu_Iris_Model_Parser_Inventory_Abstract {

    private $filePath;
    private $ftpHost;
    private $ftpUser;
    private $ftpPass;

    /**
     * TODO: inserire nel costruttore i parametri necessari a prendere i file
     */
    public static function fromLocalFile($filePath) {
        $parser = new FiloBlu_Iris_Model_Parser_Inventory_Csv_Default();
        $parser->filePath = $filePath;

        return $parser;
    }

    public static function fromFtpFile($filePath, $ftpHost, $ftpUser, $ftpPass) {
        $parser = new FiloBlu_Iris_Model_Parser_Inventory_Csv_Default();
        $parser->filePath = $filePath;
        $parser->ftpHost = $ftpHost;
        $parser->ftpUser = $ftpUser;
        $parser->ftpPass = $ftpPass;

        return $parser;
    }

    /**
     * legge il file csv e ritorna un oggetto 
     */
    public function readData() {
        $returnArray = array();
        $handle = fopen($this->filePath, "r");
        while (($data = fgetcsv($handle, 0, '|', '#')) !== false) {
            //inizio il parsing
            if(trim($data[0]) == ""){
                continue;
            }


            $colore = trim($data[10]);
            $taglia = strtolower(trim($data[8]));
            $sku = trim($data[0]) . "_colore_" . $colore . "_taglia_" . str_replace(" ", "_", strtolower($taglia));
            $qta = trim($data[17]);

            $curInventory = new FiloBlu_Iris_Model_Data_Inventory_Default();
            $curInventory->setIterations(0);
            $curInventory->setQty($qta);
            $curInventory->setSku($sku);
            $curInventory->setSource($this->filePath);
            $curInventory->setStoreId(0);

            $returnArray[] = $curInventory;
        }
        fclose($handle);
        return $returnArray;
    }

}

?>
