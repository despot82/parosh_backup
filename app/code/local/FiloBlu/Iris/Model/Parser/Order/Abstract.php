<?php

abstract class FiloBlu_Iris_Model_Parser_Order_Abstract implements FiloBlu_Iris_Model_Parser_Order_Interface{
    
    abstract public function getOrdersToExportForDal($statusArray);
}

