<?php

class FiloBlu_Iris_Model_Parser_Product_Csv_Default extends FiloBlu_Iris_Model_Parser_Product_Abstract {

    private $filePath;
    private $ftpHost;
    private $ftpUser;
    private $ftpPass;
    private $directory;

    /**
     * TODO: inserire nel costruttore i parametri necessari a prendere i file
     */
    public static function fromLocalFile($filePath) {
        $parser = new FiloBlu_Iris_Model_Parser_Product_Csv_Default();
        $parser->filePath = $filePath;

        return $parser;
    }

    private function isEmpty($field) {
        if (empty($field)) {
            return true;
        } else {
            return false;
        }
    }

    public function setFilePath($filePath) {
        $this->filePath = $filePath;
        return $this;
    }

    public function setDirectory($dir) {
        $this->directory = $dir;
        return $this;
    }

    public static function fromFtpFile($filePath, $ftpHost, $ftpUser, $ftpPass) {
        $parser = new FiloBlu_Iris_Model_Parser_Product_Csv_Default();
        $parser->filePath = $filePath;
        $parser->ftpHost = $ftpHost;
        $parser->ftpUser = $ftpUser;
        $parser->ftpPass = $ftpPass;

        return $parser;
    }

    /**
     * legge il file csv e ritorna un oggetto 
     */
    public function readData() {
        $returnArray = array();
        
        //FIX START: mi leggo i colori prima
        $colors = array();
//        $handleColori = fopen(Mage::getBaseDir('base').'/script/files/colori.csv', 'r');
//        while (($data = fgetcsv($handleColori, 0, ',', '"')) !== false) {
//            if(trim($data[0]) != "" && trim($data[1]) != ""){
//                $colors[intval(trim($data[0]))] = trim($data[1]);
//            }
//        }
                
        //FIX end: mi leggo i colori prima               
        
        //die("ok");
        
        $files = $this->_getFilesToBeProcessed();
        $count = 0;
        foreach ($files as $file) {
            $handle = fopen($file, "r");
            while (($data = fgetcsv($handle, 0, '#', '"')) !== false) {

                //inizio il parsing
                if(trim($data[0]) == ""){
                    continue;
                }
                
                $sku = trim($data[0]);

                $taglia = trim($data[1]);
                $nome_ita = trim($data[2]);
                $nome_eng = trim($data[3]);
                $colore = trim($data[6]);//$colors[intval(trim($data[4]))];
                $color_code = trim($data[4]);
                $stagione = trim($data[5]);
                $lavorazione = trim($data[7]);
                
//                $descrizione_it = trim($data[7]);
//                $descrizione_en = trim($data[8]);
//                $prezzo = trim($data[9]);
//                $qty = trim($data[12]);
//                $peso = "1";      
                
                /**
                 * Davide 10/07/2014
                 * Il tracciato è stato modificato dal cliente
                 * Francesco 09/07/2015
                 * Il tracciato è stato modificato dal cliente
                 */                
                $categoria = trim($data[9]);     //campo nuovo
                $macroCategoria = trim($data[8]);     //campo nuovo
                $descrizione_it = trim($data[10]);
                $dettagli_it = trim($data[11]);   //campo nuovo
                $descrizione_en = trim($data[12]);
                $dettagli_en = trim($data[13]);  //campo nuovo
                $prezzo = str_replace('€','',$data[14]);
                $prezzo = str_replace(".", "", $prezzo);
                $prezzo = preg_replace('/\D+([0-9]+)[,\.]([0-9]{2})/', '$1.$2', $prezzo);
                $prezzo = str_replace(",", ".", $prezzo);
                $qty = trim($data[17]); //non utilizzato
                $peso = trim($data[16]);

                $curProduct = new FiloBlu_Iris_Model_Data_Product_Default();
                $curProduct->setSku($sku);

                $curProduct->setSource("filepath");
                $curProduct->setProductType(Mage_Catalog_Model_Product_Type::TYPE_SIMPLE);

                $curProduct->setAttributeSetId(FiloBlu_Iris_Model_Data_Product_Default::ATTRIBUTE_SET_ID_DEFAULT);
                $curProduct->setWebsiteIds("1");

                $parent_sku = str_replace(" ", "_", $nome_eng)."_".$color_code;
                $curProduct->setParentSku($parent_sku);
                
                $curProduct->setConfigurableAttributes("size");

                $curProduct->setProductStatus(FiloBlu_Iris_Model_Data_Product_Default::STATUS_ENABLED);

                //siamo in un semplice che fa parte del configurabile
                $curProduct->setVisibility(1);

                $curProduct->setName($nome_ita);
                
                $curProduct->setCategoryIds($categoria.','.$macroCategoria);

                //taxClass
                $curProduct->setTaxClass(2);

                //WEIGHT
                $weight = new FiloBlu_Iris_Model_Data_Product_Attribute("weight", "text", $peso);
                $curProduct->addAttribute($weight);

                //PREZZO
                $price = new FiloBlu_Iris_Model_Data_Product_Attribute("price", "price", $prezzo);
                $curProduct->addAttribute($price);
                
                //PREZZO SPECIALE
//                $special_price = new FiloBlu_Iris_Model_Data_Product_Attribute("special_price", "price", $prezzo_speciale);
//                $curProduct->addAttribute($special_price);    
                
                //NAME ENG
                if (!$this->isEmpty($nome_eng)) {
                    $attName = new FiloBlu_Iris_Model_Data_Product_Attribute("name", "text", $nome_eng, 2);
                    $curProduct->addAttribute($attName);
                }                   

                //DESCRIZIONE_BREVE
                if (!$this->isEmpty($descrizione_it)) {
                    $descrizione_breve = new FiloBlu_Iris_Model_Data_Product_Attribute("short_description", "textarea", $descrizione_it);
                    $curProduct->addAttribute($descrizione_breve);
                }

                //Sostituito da colonna apposita nel file (Davide 10/07/2014)
//                //DESCRIZIONE
//                if (!$this->isEmpty($descrizione_it)) {
//
//                    $attDescrizione = new FiloBlu_Iris_Model_Data_Product_Attribute("description", "textarea", $descrizione_it);
//                    $curProduct->addAttribute($attDescrizione);
//                }
                
                //DETTAGLIO IT (Davide 10/07/2014)
                if (!$this->isEmpty($dettagli_it)) {
                    $dettagli_it = str_replace(';', '<br/>', $dettagli_it); //replace su richiesta del cliente
                    $attDescrizione = new FiloBlu_Iris_Model_Data_Product_Attribute("description", "textarea", $dettagli_it);
                    $curProduct->addAttribute($attDescrizione);
                }
                                
                //DESCRIZIONE_BREVE
                if (!$this->isEmpty($descrizione_en)) {
                    $descrizione_breve = new FiloBlu_Iris_Model_Data_Product_Attribute("short_description", "textarea", $descrizione_en, 2);
                    $curProduct->addAttribute($descrizione_breve);
                }

                //Sostituito da colonna apposita nel file (Davide 10/07/2014)
//                //DESCRIZIONE
//                if (!$this->isEmpty($descrizione_en)) {
//
//                    $attDescrizione = new FiloBlu_Iris_Model_Data_Product_Attribute("description", "textarea", $descrizione_en, 2);
//                    $curProduct->addAttribute($attDescrizione);
//                }                

                //DETTAGLIO EN (Davide 10/07/2014)
                if (!$this->isEmpty($dettagli_en)) {
                    $dettagli_en = str_replace(';', '<br>', $dettagli_en); //replace su richiesta del cliente
                    $attDescrizione = new FiloBlu_Iris_Model_Data_Product_Attribute("description", "textarea", $dettagli_en, 2);
                    $curProduct->addAttribute($attDescrizione);
                }                
                
                //COLORE (Decommentato Davide 10/07/2014)
                if (!$this->isEmpty($colore)) {
                    $attColore = new FiloBlu_Iris_Model_Data_Product_Attribute("color", "select", ucfirst(strtolower($colore)));
                    $curProduct->addAttribute($attColore);
                }

                //TAGLIA

                if (!$this->isEmpty($taglia)) {
                    $attTaglia = new FiloBlu_Iris_Model_Data_Product_Attribute("size", "select", $taglia);
                    $curProduct->addAttribute($attTaglia);
                }

                
                //stagione
                if (!$this->isEmpty($stagione)) {
                    $attStagione = new FiloBlu_Iris_Model_Data_Product_Attribute("stagione", "select", $stagione);
                    $curProduct->addAttribute($attStagione);
                }                
                
                //lavorazione
                if (!$this->isEmpty($lavorazione)) {
                    $attLavorazione = new FiloBlu_Iris_Model_Data_Product_Attribute("lavorazione", "multiselect", $lavorazione);
                    $curProduct->addAttribute($attLavorazione);
                }   
                
                
                //url key
                if (!$this->isEmpty($nome_ita) && !$this->isEmpty($colore)) {
                    $attUrl = new FiloBlu_Iris_Model_Data_Product_Attribute("url_key", "text", $nome_ita."_".$colore);
                    $curProduct->addAttribute($attUrl);
                }

                if (!$this->isEmpty($nome_eng) && !$this->isEmpty($colore)) {
                    $attUrlEng = new FiloBlu_Iris_Model_Data_Product_Attribute("url_key", "text", $nome_eng."_".$colore,2);
                    $curProduct->addAttribute($attUrlEng);
                }
                
                

                $returnArray[] = $curProduct;
                $count++;
//                if ($count == 100) {
//                    return $returnArray;
//                }
            }
            fclose($handle);
        }
        return $returnArray;
    }

    protected function _getFilesToBeProcessed() {
        $files = array();

        if ($this->filePath)
            $files[] = $this->filePath;

        if ($this->directory) {
            foreach (scandir($this->directory) as $file) {
                if (is_file($this->directory . $file) && filesize($this->directory . $file) > 0)
                    $files[] = $this->directory . $file;
            }
        }

        return $files;
    }

}

?>
