<?php

class FiloBlu_Iris_Model_Parser_Product_Seoupdate {

    const ALL = 1;
    const SKUS = 2;

    public function toOptionArray() {

        return array(
            array(
                'value' => self::ALL,
                'label' => Mage::helper('iris')->__('ALL')
            ),
            array(
                'value' => self::SKUS,
                'label' => Mage::helper('iris')->__('SKUS')
            )
        );
    }

}