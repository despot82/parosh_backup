<?php

abstract class FiloBlu_Iris_Model_Validator_Product_Abstract implements FiloBlu_Iris_Model_Validator_Product_Interface{
    
    abstract public function validateData(FiloBlu_Iris_Model_Data_Product_Default $object);
    
    public function utility(){
        return "data utility";
    }    
}

