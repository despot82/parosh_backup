<?php


$installer = $this;
/* @var $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();


$installer->run("
    ALTER TABLE {$installer->getTable('iris/action_order_rows')} ADD COLUMN base_gift_amount decimal(12,4);
    ALTER TABLE {$installer->getTable('iris/action_order_rows')} ADD COLUMN gift_amount decimal(12,4);
");


$installer->endSetup();
