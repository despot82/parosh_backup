<?php

class FiloBlu_Promofix_Model_Default extends Mage_Core_Model_Abstract {
    
    /**
     * Applies all promo rules
     */
    public function apply() {
        Mage::log("PROMOFIX: Applying all promo rules", null, "promofix.log");
        Mage::getModel('catalogrule/observer')->applyAllRules(null);
    }
    
}