<?php
class Parosh_Box_Block_Image extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    protected function _construct()
    {
		$template = $this->getData('template');
		if (empty($template))
			$this->setTemplate($template);

		parent::_construct();
    }

	public function getTitle()
	{
		$title = $this->getData('title');
		return !empty($title) ? $title : '';
	}

	public function getImage()
	{
		$image = $this->getData('image');
		return !empty($image) ? Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).$image : '';
	}

	public function getUrl($route = '', $params = array())
	{
		$url = $this->getData('url');
		if (!preg_match("/^(ht|f)tps?:\/\//", $url))
			$url = Mage::getBaseUrl().$url;

		return !empty($url) ? $url : '';
	}
}
?>