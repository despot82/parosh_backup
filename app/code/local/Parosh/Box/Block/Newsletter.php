<?php
class Parosh_Box_Block_Newsletter extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    protected function _construct()
    {
		$this->setTemplate('box/newsletter.phtml');

		parent::_construct();
    }
}
?>