<?php

class Parosh_Box_Block_SimpleBanner extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface {

    protected function getBanner() {

        $_banner = new Varien_Object();

        $_banner->setTitle($this->getTitle());
        $_banner->setSubTitle($this->getSubtitle());
        $_banner->setTextCta($this->getTextCta());
        $_banner->setLink($this->getLinkCta());
        $_banner->setImage($this->getImage());

        return $_banner;
    }

}

?>