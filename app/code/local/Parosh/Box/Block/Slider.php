<?php
class Parosh_Box_Block_Slider extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    protected function _construct()
    {
		$this->setTemplate('box/slider.phtml');

		parent::_construct();
    }

	public function getTitle()
	{
		$title = $this->getData('title');
		return !empty($title) ? $title : '';
	}

	public function getImages()
	{
		$images = array();

		for ($i = 1; $i <= 5; $i++)
		{
			$title = $this->getData('title_'.$i);
			$image = $this->getData('image_'.$i);
			$url = $this->getData('url_'.$i);

			if (!empty($title) && !empty($image) && !empty($url))
			{
				if (!preg_match("/^(ht|f)tps?:\/\//", $url))
					$url = Mage::getBaseUrl().$url;

				$images[] = array('title' => $title, 'image' => Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).$image, 'url' => $url);
			}
		}

		return $images;
	}
}
?>