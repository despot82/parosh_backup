<?php
class Parosh_Box_Block_Text extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    protected function _construct()
    {
		$this->setTemplate('box/text.phtml');

		parent::_construct();
    }

	public function getTitle()
	{
		$title = $this->getData('title');
		return !empty($title) ? $title : '';
	}

	public function getUrl($route = '', $params = array())
	{
		$url = $this->getData('url');
		return !empty($url) ? $url : '';
	}

	public function getText()
	{
		$text = $this->getData('text');
		return !empty($text) ? $text : '';
	}
}
?>