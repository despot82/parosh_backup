<?php
class Parosh_Catalog_Helper_Data extends Mage_Catalog_Helper_Data
{
	public function getAvailableAttributes($product)
	{
		$availableAttributes = array();

		if ($product->isConfigurable())
		{
			$usedProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null, $product);

			foreach ($usedProducts as $usedProduct)
			{
				if (!$usedProduct->isSaleable())
					continue;

				$configurableAttributes = $product->getTypeInstance(TRUE)->getConfigurableAttributes($product);

				foreach ($configurableAttributes as $attribute)
				{
					$productAttribute   = $attribute->getProductAttribute();
					$productAttributeId = $productAttribute->getId();
					$attributeValue     = $usedProduct->getData($productAttribute->getAttributeCode());

					if (!isset($availableAttributes[$productAttributeId]))
					{
						$availableAttributes[$productAttributeId] = array(
								'attribute_id'   => $productAttributeId,
								'attribute_code' => $productAttribute->getAttributeCode(),
								'store_label'    => $productAttribute->getStoreLabel(),
								'values'         => array()
							);
					}

					foreach($attribute->getPrices() as $price)
					{
						$valueIndex = $price['value_index'];

						if (isset($availableAttributes[$productAttributeId]['values'][$valueIndex]))
							continue;

						if ($attributeValue != $valueIndex)
							continue;

						$availableAttributes[$productAttributeId]['values'][$valueIndex] = $price;
					}
				}
			}
		}

		return $availableAttributes;
	}

	public function getAvailableSizes($product)
	{
		$availableSizes = array();

		$availableAttributes = $this->getAvailableAttributes($product);

		foreach ($availableAttributes as $attribute)
		{
			if ($attribute['attribute_code'] != 'size')
				continue;

			foreach($attribute['values'] as $attributeValue)
			{
				$availableSizes[] = $attributeValue;
			}
		}

		return $availableSizes;
	}
}
?>