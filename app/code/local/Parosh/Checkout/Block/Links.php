<?php

class Parosh_Checkout_Block_Links extends Mage_Checkout_Block_Links
{
    /**
     * Add shopping cart link to parent block
     *
     * @return Mage_Checkout_Block_Links
     */
    public function addCartLink()
    {
        $parentBlock = $this->getParentBlock();
        if ($parentBlock && Mage::helper('core')->isModuleOutputEnabled('Mage_Checkout')) {
            $count = $this->getSummaryQty() ? $this->getSummaryQty()
                : $this->helper('checkout/cart')->getSummaryCount();
            if ($count == 1) {
                $text = "<span class='icon-cart'></span><span class='count'>".$this->__('(%s)', $count)."</span>";
            } elseif ($count > 0) {
                $text = "<span class='icon-cart'></span><span class='count'>".$this->__('(%s)', $count)."</span>";
            } else {
                $text = "<span class='icon-cart'></span>";
            }

            $parentBlock->removeLinkByUrl($this->getUrl('checkout/cart'));
            $parentBlock->addLink($text, 'checkout/cart', $this->__('My Shopping Bag'), true, array(), 50, 'id="cart-top-link"', 'class="top-link-cart"');
        }
        return $this;
    }
}
