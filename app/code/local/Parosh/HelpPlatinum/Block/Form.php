<?php

class Parosh_HelpPlatinum_Block_Form extends Mage_Core_Block_Template
{
    const XML_PAGE_TITLE = 'helpplatinum/pagehelp/page_title';
    const XML_PAGE_SUMMARY = 'helpplatinum/pagehelp/page_summary';
    
    public function getFormData()
    {
        $data = $this->getData('form_data');
        if (is_null($data)) {
                        
            $formData = Mage::getSingleton('core/session')->getHelpPlatinumFormData(); 
            
            $data = new Varien_Object();
            if ($formData) {
                $data->addData($formData);
            } 
            $this->setData('form_data', $data);
        }

        return $data;
    }
    
    public function getTitle()
    {
        return trim(Mage::getStoreConfig(self::XML_PAGE_TITLE));
    }
    
    public function getSummary()
    {
        return nl2br(trim(Mage::getStoreConfig(self::XML_PAGE_SUMMARY)));
    }
}