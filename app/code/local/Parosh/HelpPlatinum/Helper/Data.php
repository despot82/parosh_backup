<?php

class Parosh_HelpPlatinum_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_ENABLED = 'helpplatinum/helpplatinum/enabled';
    const XML_PATH_ENABLED_DEBUG = 'helpplatinum/helpplatinum/debug_enable';
    const XML_PATH_ENABLED_DEBUG_EMAIL = 'helpplatinum/helpplatinum/debug_enable_email';
    const XML_PATH_RECIPIENT_DEBUG = 'helpplatinum/helpplatinum/debug_recipient_email';
    // Email
    const XML_PATH_EMAIL_RECIPIENT = 'helpplatinum/email/recipient_email';
    const XML_PATH_EMAIL_SENDER = 'helpplatinum/email/sender_email_identity';
    const XML_PATH_EMAIL_TEMPLATE = 'helpplatinum/email/email_template';

    public function isEnabled()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENABLED);
    }

    public function isDebug()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENABLED_DEBUG);
    }

    public function isDebugEmail()
    {
        return Mage::getStoreConfig(self::XML_PATH_ENABLED_DEBUG_EMAIL);
    }

    public function emailDebug()
    {
        return Mage::getStoreConfig(self::XML_PATH_RECIPIENT_DEBUG);
    }

    public function getPathEmailRecipient()
    {
        return Mage::getStoreConfig(self::XML_PATH_EMAIL_RECIPIENT);
    }
    
    public function getPathEmailSender()
    {
        return Mage::getStoreConfig(self::XML_PATH_EMAIL_SENDER);
    }
    
    public function getPathEmailTemplate()
    {
        return Mage::getStoreConfig(self::XML_PATH_EMAIL_TEMPLATE);
    }
}
