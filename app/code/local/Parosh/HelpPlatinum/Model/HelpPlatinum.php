<?php

class Parosh_HelpPlatinum_Model_HelpPlatinum extends Mage_Core_Model_Abstract
{
    CONST LOG_FILE = 'filoblu_helpplatinum.log';
    
    public function send()
    {
        $vars = array(
            'subject' => $this->getData('subject'),
            'ordernumber' => $this->getData('ordernumber'),
            'object' => $this->getData('object'),
            'email' => $this->getData('email'),
            'telephone' => $this->getData('telephone'),
            'hour' => $this->getData('hour')
        );
                
        if (Mage::helper('helpplatinum')->isDebug()) {
            Mage::log('send - Model Help Platinum', null, self::LOG_FILE, true);
            Mage::log($vars, null, self::LOG_FILE, true);
            Mage::log('path template: '.Mage::helper('helpplatinum')->getPathEmailTemplate(), null, self::LOG_FILE, true);
            Mage::log('sender: '.Mage::helper('helpplatinum')->getPathEmailSender(), null, self::LOG_FILE, true);
        }
        
        try {
            $emailTemplate = Mage::getModel('core/email_template');
        
            if (Mage::helper('helpplatinum')->isDebugEmail()) {
                $emailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->setReplyTo(Mage::helper('helpplatinum')->emailDebug())
                    ->setTemplateSubject($vars['subject'])
                    ->sendTransactional(
                        Mage::helper('helpplatinum')->getPathEmailTemplate(),
                        Mage::helper('helpplatinum')->getPathEmailSender(),
                        Mage::helper('helpplatinum')->emailDebug(),
                        null,
                        $vars
                    );
            } else {
                $emailTemplate->setDesignConfig(array('area' => 'frontend'))
                    ->setReplyTo(Mage::helper('helpplatinum')->getPathEmailRecipient())
                    ->setTemplateSubject($vars['subject'])
                    ->sendTransactional(
                        Mage::helper('helpplatinum')->getPathEmailTemplate(),
                        Mage::helper('helpplatinum')->getPathEmailSender(),
                        Mage::helper('helpplatinum')->getPathEmailRecipient(),
                        null,
                        $vars
                    );
            }
        } catch (Exception $ex) {
            Mage::log($ex->getMessage(), null, self::LOG_FILE, true);
            return false;
        }
        
        return true;
    }
    
    public function checkOrderNumber()
    {        
        if (! $this->getData('ordernumber')) {
            return false;
        }
        
        $order = Mage::getModel('sales/order')->loadByIncrementid($this->getData('ordernumber'));
        
        if ($order && $order->getId()) {
            return true;
        }
        
        return false;
    }
}

