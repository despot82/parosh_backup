<?php

class Parosh_HelpPlatinum_IndexController extends Mage_Core_Controller_Front_Action
{
    CONST LOG_FILE = 'filoblu_helpplatinum.log';
    
    public function preDispatch()
    {
        parent::preDispatch();

        if (!Mage::helper('helpplatinum')->isEnabled()) {
            $this->norouteAction();
        }
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('helpplatinumForm')
                ->setFormAction(Mage::getUrl('*/*/post'));

        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');
        $this->renderLayout();
    }

    public function postAction()
    {
        $post = $this->getRequest()->getPost();
        $_debug = Mage::helper('helpplatinum')->isDebug();

        if ($post) {
            if ($_debug) {
                Mage::log('POST DATA', null, self::LOG_FILE, true);
                Mage::log($post, null, self::LOG_FILE, true);
            }

            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;
                $error_msg = array();
                
                if (!Zend_Validate::is(trim($post['ordernumber']), 'NotEmpty')) {
                    $error = true;
                    $error_msg[] = Mage::helper('helpplatinum')->__('The field Order Number is empty');
                }

                if (!Zend_Validate::is(trim($post['object']), 'NotEmpty')) {
                    $error = true;
                    $error_msg[] = Mage::helper('helpplatinum')->__('The field Object is empty');
                }

                if (!Zend_Validate::is(trim($post['email']), 'NotEmpty')) {
                    $error = true;
                    $error_msg[] = Mage::helper('helpplatinum')->__('The field Email is empty');
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                    $error_msg[] = Mage::helper('helpplatinum')->__('The field Email is wrong');
                }

                if (!Zend_Validate::is(trim($post['telephone']), 'NotEmpty')) {
                    $error = true;
                    $error_msg[] = Mage::helper('helpplatinum')->__('The field Telephone is empty');
                }
                
                if (!Zend_Validate::is(trim($post['hour']), 'NotEmpty')) {
                    $error = true;
                    $error_msg[] = Mage::helper('helpplatinum')->__('The field Hour is empty');
                }
                
                if ($error) {
                    Mage::getSingleton('core/session', array('name' => 'frontend'))->setHelpPlatinumFormData($post);

                    throw new Exception();
                }

                $helpPlatinum = Mage::getModel('helpplatinum/helpPlatinum');                
                
                $helpPlatinum->setData('ordernumber', $post['ordernumber']);
                
                if (!$helpPlatinum->checkOrderNumber()) {
                    $error_msg[] = Mage::helper('helpplatinum')->__('The Order Number not exists');
                    
                    throw new Exception();
                }
                
                $helpPlatinum->setData('object', $post['object']);
                $helpPlatinum->setData('email', $post['email']);
                $helpPlatinum->setData('telephone', $post['telephone']);
                $helpPlatinum->setData('hour', $post['hour']);
                
                $helpPlatinum->setData('subject', Mage::helper('helpplatinum')->__('Help Platinum from order # %s', $post['ordernumber']));
                
                if (!$helpPlatinum->send()) {
                    throw new Exception();
                }
                
                Mage::getSingleton('core/session', array('name' => 'frontend'))->unsHelpPlatinumFormData();
                
                Mage::getSingleton('customer/session')->addSuccess(Mage::helper('helpplatinum')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
                
                $this->_redirect('*/*/');
                return;
                
            } catch (Exception $ex) {
                
                Mage::log($ex->getMessage(), null, self::LOG_FILE, true);
                
                Mage::getSingleton('core/session', array('name' => 'frontend'))->setHelpPlatinumFormData($post);
                
                if (count($error_msg) > 0) {
                    Mage::getSingleton('core/session', array('name' => 'frontend'))->addError(implode('<br />', $error_msg));
                } else {
                    Mage::getSingleton('core/session', array('name' => 'frontend'))->addError(Mage::helper('helpplatinum')->__('Unable to submit your request. Please, try again later'));
                }
                
                $this->_redirect('*/*/');
                return;
            }
        } else {            
            $this->_redirect('*/*/');
        }
    }

}