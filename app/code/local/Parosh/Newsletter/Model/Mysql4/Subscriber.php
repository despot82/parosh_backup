<?php
class Parosh_Newsletter_Model_Mysql4_Subscriber extends Mage_Newsletter_Model_Mysql4_Subscriber
{
	protected function _prepareSave(Parosh_Newsletter_Model_Subscriber $subscriber)
	{
		$data = array();
		$data['customer_id'] = $subscriber->getCustomerId();
		$data['store_id'] = $subscriber->getStoreId() ? $subscriber->getStoreId() : 0;
		$data['subscriber_email']  = $subscriber->getEmail();
		$data['subscriber_status'] = $subscriber->getStatus();
	    $data['subscriber_confirm_code'] = $subscriber->getCode();

		$data['subscriber_firstname'] = $subscriber->getFirstname();
		$data['subscriber_lastname'] = $subscriber->getLastname();
		$data['subscriber_date_of_birth'] = $subscriber->getDateOfBirth();
		$data['subscriber_country'] = $subscriber->setCountry();
	}
}
