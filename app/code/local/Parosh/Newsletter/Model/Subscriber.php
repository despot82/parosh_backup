<?php
class Parosh_Newsletter_Model_Subscriber extends Mage_Newsletter_Model_Subscriber
{
    public function subscribe($email, $firstName = NULL, $lastName = NULL, $dateOfBirth = NULL, $country = NULL)
    {
		$this->loadByEmail($email);
        $customerSession = Mage::getSingleton('customer/session');

        if(!$this->getId())
		{
            $this->setSubscriberConfirmCode($this->randomSequence());
        }

        $isConfirmNeed   = (Mage::getStoreConfig(self::XML_PATH_CONFIRMATION_FLAG) == 1) ? true : false;
        $isOwnSubscribes = false;
        $ownerId = Mage::getModel('customer/customer')
            ->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
            ->loadByEmail($email)
            ->getId();

        $isSubscribeOwnEmail = $customerSession->isLoggedIn() && $ownerId == $customerSession->getId();

        if (!$this->getId() || $this->getStatus() == self::STATUS_UNSUBSCRIBED || $this->getStatus() == self::STATUS_NOT_ACTIVE)
		{
            if ($isConfirmNeed === true)
			{
                // if user subscribes own login email - confirmation is not needed
                $isOwnSubscribes = $isSubscribeOwnEmail;
                if ($isOwnSubscribes == true)
				{
					$this->setStatus(self::STATUS_SUBSCRIBED);
                }
				else
				{
                    $this->setStatus(self::STATUS_NOT_ACTIVE);
                }
            }
			else
			{
                $this->setStatus(self::STATUS_SUBSCRIBED);
            }

            $this->setSubscriberEmail($email);

        }

        if ($isSubscribeOwnEmail)
		{
			$customer = $customerSession->getCustomer();

			$this->setStoreId($customer->getStoreId());
            $this->setCustomerId($customer->getId());
			$this->setSubscriberFirstname($customer->getFirstname());
			$this->setSubscriberLastname($customer->getLastname());
			$this->setSubscriberDateOfBirth($customer->getDob());
        }
		else
		{
            $this->setStoreId(Mage::app()->getStore()->getId());
            $this->setCustomerId(0);
			$this->setSubscriberFirstname($firstName);
			$this->setSubscriberLastname($lastName);

			if ($dateOfBirth)
			{
				$this->setSubscriberDateOfBirth($dateOfBirth);
			}

			if ($country)
			{
				$this->setSubscriberCountry($country);
			}
        }

        $this->setIsStatusChanged(true);

        try
		{
            $this->save();

            if ($isConfirmNeed === true && $isOwnSubscribes === false)
			{
                $this->sendConfirmationRequestEmail();
            }
			else
			{
                $this->sendConfirmationSuccessEmail();
            }

            return $this->getStatus();
        }
		catch (Exception $e)
		{
            throw new Exception($e->getMessage());
        }
    }

	public function subscribeCustomer($customer)
    {
        $this->loadByCustomer($customer);

        if ($customer->getImportMode()) {
            $this->setImportMode(true);
        }

        if (!$customer->getIsSubscribed() && !$this->getId())
		{
            return $this;
        }

        if(!$this->getId())
		{
            $this->setSubscriberConfirmCode($this->randomSequence());
        }

       $confirmation = null;

       if ($customer->isConfirmationRequired() && ($customer->getConfirmation() != $customer->getPassword()))
	   {
           $confirmation = $customer->getConfirmation();
       }

        $sendInformationEmail = false;

        if ($customer->hasIsSubscribed())
		{
            $status = $customer->getIsSubscribed()
                ? (!is_null($confirmation) ? self::STATUS_UNCONFIRMED : self::STATUS_SUBSCRIBED)
                : self::STATUS_UNSUBSCRIBED;

            if ($status != self::STATUS_UNCONFIRMED && $status != $this->getStatus())
			{
                $sendInformationEmail = true;
            }
        }
		elseif (($this->getStatus() == self::STATUS_UNCONFIRMED) && (is_null($confirmation)))
		{
            $status = self::STATUS_SUBSCRIBED;
            $sendInformationEmail = true;
        }
		else
		{
            $status = ($this->getStatus() == self::STATUS_NOT_ACTIVE ? self::STATUS_UNSUBSCRIBED : $this->getStatus());
        }

        if($status != $this->getStatus())
		{
            $this->setIsStatusChanged(true);
        }

        $this->setStatus($status);

        if(!$this->getId())
		{
            $storeId = $customer->getStoreId();

            if ($customer->getStoreId() == 0)
			{
                $storeId = Mage::app()->getWebsite($customer->getWebsiteId())->getDefaultStore()->getId();
            }

            $this->setStoreId($storeId)
                ->setCustomerId($customer->getId())
                ->setEmail($customer->getEmail());
        }
		else
		{
            $this->setStoreId($customer->getStoreId())
                ->setEmail($customer->getEmail());
        }

		$this->setSubscriberFirstname($customer->getFirstname());
		$this->setSubscriberLastname($customer->getLastname());
		$this->setSubscriberDateOfBirth($customer->getDob());

        $this->save();

        $sendSubscription = $customer->getData('sendSubscription') || $sendInformationEmail;
        if (is_null($sendSubscription) xor $sendSubscription)
		{
            if ($this->getIsStatusChanged() && $status == self::STATUS_UNSUBSCRIBED)
			{
                $this->sendUnsubscriptionEmail();
            }
			elseif ($this->getIsStatusChanged() && $status == self::STATUS_SUBSCRIBED)
			{
                $this->sendConfirmationSuccessEmail();
            }
        }

        return $this;
    }
}