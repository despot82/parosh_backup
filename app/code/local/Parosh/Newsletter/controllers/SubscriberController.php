<?php

require_once 'Mage/Newsletter/controllers/SubscriberController.php';

class Parosh_Newsletter_SubscriberController extends Mage_Newsletter_SubscriberController {

    public function newAction() {
        if ($this->getRequest()->isPost() && $this->getRequest()->getPost('email')) {
            $session = Mage::getSingleton('core/session');
            $customerSession = Mage::getSingleton('customer/session');
            $firstName = (string) $this->getRequest()->getPost('firstname');
            $lastName = (string) $this->getRequest()->getPost('lastname');
            $email = (string) $this->getRequest()->getPost('email');
            $dateOfBirth = (string) $this->getRequest()->getPost('dob_full');
            $country = (string) $this->getRequest()->getPost('country');
            $acceptPrivacy = (int) $this->getRequest()->getPost('privacy', 0);
            
            try {
                if (!Zend_Validate::is($firstName, 'NotEmpty')) {
                    Mage::throwException($this->__('Please enter a valid first name.'));
                }

                if (!Zend_Validate::is($email, 'EmailAddress')) {
                    Mage::throwException($this->__('Please enter a valid email address.'));
                }

                if ($dateOfBirth) {
                    if (!Zend_Validate::is($dateOfBirth, 'Date', array('format' => 'yyyy-mm-dd'))) {
                        Mage::throwException($this->__('Please enter a valid date of birth.'));
                    }
                }

                if (!intval($acceptPrivacy)) {
                    Mage::throwException($this->__('Your must accept Privacy Policy.'));
                }

                if (Mage::getStoreConfig(Mage_Newsletter_Model_Subscriber::XML_PATH_ALLOW_GUEST_SUBSCRIBE_FLAG) != 1 && !$customerSession->isLoggedIn()) {
                    Mage::throwException($this->__('Sorry, but administrator denied subscription for guests. Please <a href="%s">register</a>.', Mage::helper('customer')->getRegisterUrl()));
                }
                
                $subscriberId = Mage::getModel('newsletter/subscriber')->loadByEmail($email)->getId();
                
                if ($subscriberId !== null && $subscriberId != $customerSession->getId()) {
                    Mage::throwException($this->__('This email address is already assigned to another user.'));
                }

                $status = Mage::getModel('newsletter/subscriber')->subscribe($email, $firstName, $lastName, $dateOfBirth, $country);
                if ($status == Mage_Newsletter_Model_Subscriber::STATUS_NOT_ACTIVE) {
                    $session->addSuccess($this->__('Confirmation request has been sent.'));
                } else {
                    $session->addSuccess($this->__('Thank you for your subscription.'));
                }
            } catch (Mage_Core_Exception $e) {
                $session->addException($e, $this->__('There was a problem with the subscription: %s', $e->getMessage()));
            } catch (Exception $e) {
                $session->addException($e, $this->__('There was a problem with the subscription.'));
            }
        }

        $this->_redirectReferer();
    }

}
