<?php
$installer = $this;

$installer->startSetup();

$installer->run("
	ALTER TABLE {$this->getTable('newsletter_subscriber')}
	ADD
	(
		`subscriber_firstname` TEXT NULL,
		`subscriber_lastname` TEXT NULL,
		`subscriber_date_of_birth` TIMESTAMP NULL,
		`subscriber_country` TEXT NULL
	);
");

$installer->endSetup();
