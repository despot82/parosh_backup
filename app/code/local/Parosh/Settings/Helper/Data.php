<?php
class Parosh_Settings_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function getMediaUrl($file)
	{
		return !empty($file) ? Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA).'parosh/'.$file : '';
	}
}
?>