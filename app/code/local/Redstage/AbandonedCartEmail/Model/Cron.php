<?php

class Redstage_AbandonedCartEmail_Model_Cron {

	const XML_PATH_TEMPLATE						= 'abandonedcartemail/settings/template';
	const XML_PATH_REMINDER_PERIOD				= 'abandonedcartemail/settings/reminder_period';
	const XML_PATH_EMAIL_IDENTITY				= 'abandonedcartemail/settings/identity';
	const XML_PATH_EMAIL_COPY_TO				= 'abandonedcartemail/settings/email_copy_to';
	const XML_PATH_EMAIL_OVERRIDE				= 'abandonedcartemail/settings/email_override';

	public function checkAbandonedCarts( $observer ){ // The checker

		// Load helper
		$helper = Mage::helper('abandonedcartemail');
		$helper->log( 'Running Cron: '. Mage::getModel('core/date')->date() );

		// Load enabled
		$enabled = Mage::getStoreConfig('abandonedcartemail/settings/enabled');
		if( !$enabled ){
			$helper->log( 'ERROR - Module not enabled' );
			return false;
		}

		// Load template ID
		$email_template_id = Mage::getStoreConfig( self::XML_PATH_TEMPLATE );
		if( !$email_template_id ){
			$helper->log( 'ERROR - Email Template NOT FOUND' );
			return false;
		}

		// Load reminder period
		$reminder_period = Mage::getStoreConfig( self::XML_PATH_REMINDER_PERIOD );
		if( !$reminder_period ){
			$helper->log( 'ERROR - Reminder Period NOT FOUND' );
			return false;
		}

		// Load abandoned carts
		$abandoned_carts = Mage::getResourceModel('reports/quote_collection');
		$abandoned_carts->prepareForAbandonedReport('');

		// Filter out the carts that we have already processed -- we don't want to be annoying
		$tableName = Mage::getSingleton('core/resource')->getTableName('abandonedcartemail_log');
		$abandoned_carts->getSelect()->joinLeft( $tableName, $tableName .".quote_id=main_table.entity_id", array( 'abandoned_log_id' => $tableName .'.log_id', 'quote_id' => $tableName .'.quote_id' ) );
		//$abandoned_carts->getSelect()->where( ' '. $tableName .'.quote_id IS NULL ' );
		$abandoned_carts->getSelect()->where( '
			( '. $tableName .'.quote_id IS NULL )
			OR( '. $tableName .'.quote_id IS NOT NULL AND main_table.updated_at > '. $tableName .'.processed_date )
		' );

		// For every valid abandoned cart...
		foreach( $abandoned_carts as $abandoned_cart ){

			// If the cart is not abandoned yet, skip
			if(
				( strtotime( $abandoned_cart->getUpdatedAt() ) + ( $reminder_period * 3600 ) ) // cart last updated + grace period in seconds
				> strtotime( Mage::getModel('core/date')->gmtDate() ) // current timestamp
			){
				continue;
			}

			$helper->log( 'processing '. $abandoned_cart->getId() );

			//$helper->log( 'Found abandoned cart with quote_id: '. $abandoned_cart->getId() );

			// Prepare the email object
			Mage::getSingleton('core/translate')->setTranslateInline(false);
			$mailTemplate 	= Mage::getModel('core/email_template');

			// Set admins to receive a copy
			$copyTo = $this->_getEmails( self::XML_PATH_EMAIL_COPY_TO );
			if( $copyTo ){
				foreach( $copyTo as $email ){
					$mailTemplate->addBcc( $email );
				}
			}

			$helper->log( 'before sending email' );

			// Determine the recipient
			$recipient = $abandoned_cart->getEmail();
			if( $email_override = Mage::getStoreConfig( self::XML_PATH_EMAIL_OVERRIDE ) ){
				$recipient = $email_override;
			}

			$helper->log( 'recipient: '. $recipient );

			// Send the email!
			$mailTemplate->setDesignConfig( array( 'area' => 'frontend', 'store' => 1 ) )
				->sendTransactional(
					$email_template_id, // Email Template ID
					Mage::getStoreConfig( self::XML_PATH_EMAIL_IDENTITY ), // Sender
					$recipient, // Recipient Email
					$abandoned_cart->getCustomerName(), // Recipient Name
					array(
						'cart' => $abandoned_cart,
						'storename' => Mage::app()->getStore()->getGroup()->getName()
					)
				);

			Mage::getSingleton('core/translate')->setTranslateInline(true);

			if( !$mailTemplate->getSentSuccess() ){
				$helper->log( 'ERROR - failed to send customer email' );
			} else {

				$log = Mage::getModel('abandonedcartemail/log');
				if( $abandoned_log_id = $abandoned_cart->getAbandonedLogId() ){
					$helper->log( 'this cart was previously abandoned' );
					$log->load( $abandoned_log_id );
				}
				$log
					->setQuoteId( $abandoned_cart->getId() )
					->setIsProcessed( 1 )
					->setProcessedDate( Mage::getModel('core/date')->gmtDate() );
				$log->save();

			}

		}

		return;

	}

	protected function _getEmails( $configPath ){

		$data = Mage::getStoreConfig( $configPath );

		if( !empty( $data ) ){
			return explode( ',', $data );
		}

		return false;

	}

}

?>