<?php

$installer = $this;

$installer->startSetup();

$installer->run("

DROP TABLE IF EXISTS {$this->getTable('abandonedcartemail_log')};

CREATE TABLE {$this->getTable('abandonedcartemail_log')} (
  `log_id` INT(11) unsigned NOT NULL AUTO_INCREMENT,
  `quote_id` INT(11) NOT NULL,
  `is_processed` TINYINT(1) DEFAULT NULL,
  `processed_date` DATETIME DEFAULT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	");

$installer->run("
	INSERT INTO {$this->getTable('abandonedcartemail_log')} (
		quote_id,
		is_processed,
		processed_date
	) (
		SELECT
			`main_table`.`entity_id`,
			1,
			NOW()
		FROM
			{$this->getTable('sales/quote')} AS `main_table`
	)
");

$installer->endSetup();

?>