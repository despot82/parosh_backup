<?php

class Thinkopen_Cronstockupdater_Helper_Data extends Mage_Core_Helper_Abstract
{     
   
	public function updateSkusStockPerCsv($fileName, $delimiter = ";") {
		if(!file_exists($fileName) || !is_readable($fileName)) {
			return false;
		}
		
		if(($handle = fopen($fileName, 'r')) !== false) {
			
			$cummulativeQuantity = array(); // Array to store stock quantites for products
			
			// Traverse the CSV
			while((($row = fgetcsv($handle, 3000, $delimiter)) !== false)) {
				
				//error_log(print_r($row, 1));
				
				if(!$header) {
					$header = $row;
					
				} else {
					
					$productModel = Mage::getModel('catalog/product');
					$row = array_combine($header, $row);
					
					Mage::log("row array: " . print_r($row, 1));
					if(count(array_keys($header)) != count(array_keys($row))) break; //If number of fields doesn't match
					
					//Get data from the CSV row
					$sku = $row["BARCODE"];
					$warehouse = $row["WAREHOUSE"];
					$swq = $row["QTA"]; // sku - warehouse - quantity (quantity of that sku in that warehouse
					
					if(empty($cummulativeQuantity[$sku])) $cummulativeQuantity[$sku] = 0;
					
					$cummulativeQuantity[$sku] += $swq;// Account for this WH qty of the product, for later save
					
					$this -> saveSkuWarehouseQuantity($sku, $warehouse, $swq);
				}
			} // END Traverse CSV
			
			foreach ($cummulativeQuantity as $sku => $totalQuantity) {
				$this -> saveCummulativeSkuQuantity($sku, $totalQuantity);
			}
		}
	}
	
	//Write the quantity of the product in one specific warehouse
	private function saveSkuWarehouseQuantity($sku, $warehouse, $quantity) {
		$productModel = Mage::getModel('catalog/product');
		$productId = $productModel -> getIdBySku($sku); // Fetch product id by sku
		
		$warehouseId = $this -> getWarehouseIdByWarehouseCode($warehouse);
		
		Mage::getModel('wgmulti/warehouse_product')
		->getCollection() //get the collection
		->addWarehouseIdFilter($warehouseId) //filter by warehouse id
		->addProductIdFilter($sku) //filter by product id
		->getFirstItem() // if doesn't exist, return new object
		->setWarehouseId($warehouseId) // set the warehouse id
		->setProductId($productId) // set the product id
		->setQty($quantity) // set the quantity for the product and warehouse
		->save(); // save it
		Mage::log("At the end of saveSkuWarehouseQuantity");
	}
	
	private function saveCummulativeSkuQuantity ($sku, $quantity) {
		
		$productModel = Mage::getModel('catalog/product');
		
		$productId = $productModel -> getIdBySku($sku); //Get product id by sku
		
		try {
		    $product = $productModel -> load($productId);
		} catch(Exception $ex) {
			Mage::log("Product couldn't be loaded - in Cronstockupdater - Data.php.");
		}
		
		$stockData = $product -> getStockData();
		$stockData["qty"] = $quantity;
		$product -> setStockData($stockData);
		
		try {
			$product->save();
		}
		catch (Exception $ex) {
			echo $ex->getMessage();
			Mage::log("Error happened in Cronstockupdater - Data.php during saving of the product");
		}
		
		Mage::log("At the end of saveCummulativeSkuQuantity");
		 
	}
	
	private function getWarehouseIdByWarehouseCode($code) {
		
		$model = Mage::getModel('wgmulti/warehouse');
		return $model -> getCollection() -> addFieldToFilter('code', $code) -> getData()[0]["id"];
		
	}
	
	
}

?>