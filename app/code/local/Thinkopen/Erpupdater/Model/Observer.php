<?php
	class Thinkopen_Erpupdater_Model_Observer{
		
		//sales_model_service_quote_submit_after
		public function exportOrder(Varien_Event_Observer $observer) 
		{
			$order = $observer -> getOrder();
		

			$orderId = $order -> getId();
			
			$orderDateDataExploded = explode("-", explode(" ", $order -> getCreatedAt())[0]);
			
			$date = $orderDateDataExploded[2] . $orderDateDataExploded[1] . $orderDateDataExploded[0];
			
			//Go through configurable AND simple of each configurable
			foreach ($order -> getAllItems() as $item){ 
				
				$sku = $item -> getSku();
				
				//Simple(not configurable) has additional data for warehouse qty-s
				if($item -> getProductType() == "simple") {
					
                    $price .= Mage::getModel("sales/order_item") -> load($item -> getParentItemId()) -> getPrice();
                    
                    //Check if item is multiwarehouse item
                    $mwhQuantities = unserialize($item -> getAdditionalData());
                    if(empty($mwhQuantities)) {
                    	$qty = $item -> getQtyOrdered();
                    	$this -> doWriting($orderId, $date, $sku, $qty, $price, 0);
                    } else {
                    	foreach($mwhQuantities as $mwhId => $mwhQuantity) {
                    		$this -> doWriting($orderId, $date, $sku, $mwhQuantity, $price, $mwhId);
                    	}
                    
                    }
				    
                }
				
			}
			
		} //END exportOrder

		private function doWriting($orderId, $date, $sku, $qty, $price, $wh = 0) {
			
			//Get configuration data for the name of files and their first columns
			$location1 = Mage::getStoreConfig("erpupdater/erp_settings/location_1");
			$fixedColumnName1 = Mage::getStoreConfig("erpupdater/erp_settings/fixed_column_name_1");
			$location2 = Mage::getStoreConfig("erpupdater/erp_settings/location_2");
			$fixedColumnName2 = Mage::getStoreConfig("erpupdater/erp_settings/fixed_column_name_2");
			
			$toWrite1 = $fixedColumnName1 . ";";
			$toWrite1 .= "Eshop " . $orderId . ";";
			$toWrite1 .= $date . ";";
			$toWrite1 .= $sku . ";";
			$toWrite1 .= $qty . ";";
			$toWrite1 .= $price . ";";
			$toWrite1 .= $this -> getWarehouseCodeByWarehouseId($wh) . "\n";
			
			$toWrite2 = $fixedColumnName2 . ";";
			$toWrite2 .= "Eshop " . $orderId . ";";
			$toWrite2 .= $date . ";";
			$toWrite2 .= $sku . ";";
			$toWrite2 .= $qty . ";";
			$toWrite2 .= $price . ";";
			$toWrite2 .= $this -> getWarehouseCodeByWarehouseId($wh) . "\n";
			
			$exportFolder = "/export/erp_exports";
				
			// Export should happen in media folder. Folder should be created if it doesn't exist.
			$exportFolder = Mage::getBaseDir('var') . $exportFolder;
				
			if(!file_exists($exportFolder)) {
				mkdir($exportFolder, 0777, true);
			}
				
			$handle1 = fopen($exportFolder . "/" . $location1, "a+");
			$handle2 = fopen($exportFolder . "/" . $location2, "a+");
			

			fwrite($handle1, $toWrite1);
			fwrite($handle2, $toWrite2);
			
			fclose($handle1);
			fclose($handle2);

			
		}
		
		private function getWarehouseCodeByWarehouseId($id) {

            $model = Mage::getModel('wgmulti/warehouse');

		    return $model -> getCollection() -> addFieldToFilter('id', $id) -> getData()[0]["code"];

 	    }
		
	}

?>