<?php
class Webgriffe_BancaSellaGw_Block_Sales_Order_View extends Mage_Adminhtml_Block_Sales_Order_View 
{
	#### DEPRECATED ####
	### NO MORE USED ###
	public function __construct()
	{
		parent::__construct();

		//=====Get the order_id
		$orderId=$this->getRequest()->getParam('order_id');
		
		//Get the order object
		$order = Mage::getSingleton('sales/order');
		$order->load($orderId);	    

		$paymentMethod = ($order->getPayment() ? $order->getPayment()->getMethod() : '');

		//Get the order state
		if($order->getStatus() == Mage_Sales_Model_Order::STATE_PENDING_PAYMENT
			&& !strcasecmp($paymentMethod, 'bancasellagw'))
		{
			$this->_addButton(
		    	'order_newaction', 
				array(
					'label' => Mage::helper('core')->__('Repeat payment'),
					'onclick' => 'setLocation(\'' . $this->getUrl('bancasellagw/return/adminRepay') . '\')',
				)
			);
		}
  }
}