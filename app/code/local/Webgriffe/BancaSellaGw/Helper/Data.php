<?php
class Webgriffe_BancaSellaGw_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function log()
    {
        $args = func_get_args();
        $formattedMsg = call_user_func_array('sprintf', $args);
        Mage::log($formattedMsg, null, 'BancaSellaGw.log', $this->_getConfig('debug'));
    }

    public function getSession()
    {
        return Mage::getSingleton('bancasellagw/session');
    }

    //
    // Config: ORDER NEW
    //
    public function getOrderNewState($storeId = null) {
        return $this->loadStateFromStatus($this->getOrderNewStatus($storeId));
    }

    public function getOrderNewStatus($storeId = null) {
        return $this->_getConfig('order_new_state', $storeId);
    }

    //
    // Config: ORDER SUCCESS
    //
    public function getOrderSuccessState($storeId = null) {
        return $this->loadStateFromStatus($this->getOrderSuccessStatus($storeId));
    }

    public function getOrderSuccessStatus($storeId = null) {
        return $this->_getConfig('order_success_state', $storeId);
    }

    //
    // Config: ORDER FAULT
    //
    public function getOrderFaultState($storeId = null) {
        return $this->loadStateFromStatus($this->getOrderFaultStatus($storeId));
    }

    public function getOrderFaultStatus($storeId = null) {
        return $this->_getConfig('order_fault_state', $storeId);
    }

    //
    // Config: ORDER CANCELED
    //
    public function getOrderCanceledState($storeId = null) {
        return $this->loadStateFromStatus($this->getOrderCanceledStatus($storeId));
    }

    public function getOrderCanceledStatus($storeId = null) {
        return $this->_getConfig('order_canceled_state', $storeId);
    }

    public function getOrderGenerateInvoice($storeId = null) {
        return $this->_getConfig('generate_invoice', $storeId);
    }

    public function getOrderNotifyCustomer($storeId = null) {
        return $this->_getConfig('notify_customer', $storeId);
    }

    /**
     * @param Mage_Core_Controller_Request_Http $request
     * @return Mage_Sales_Model_Order
     */
    public function processResponse(Mage_Core_Controller_Request_Http $request)
    {
        $isOrderSuccessful = false;

        $requestParamA = $request->getParam('a');
        $requestParamB = $request->getParam('b');

        //Decrypt the response
        $thePaymentMethod = Mage::getModel('bancasellagw/paymentMethod');
        $params = array(
            'shopLogin' => $requestParamA,
            'CryptedString' => $requestParamB,
        );
        $thePaymentMethod->decrypt($params);

        //ShopTransactionID (Order #)
        $orderIncrementId = $thePaymentMethod->decryptedOrderId;

        //Load the Order obj
        $order = Mage::getModel('sales/order')
            ->loadByIncrementId($orderIncrementId);

        if ($order->getId()) {

            //TransactionResult
            $esito = $thePaymentMethod->decryptedResponse;

            //ErrorCode
            $errorCode = $thePaymentMethod->decryptErrorCode;

            switch ($esito) {
                case Webgriffe_BancaSellaGw_Model_PaymentMethod::TRANS_RESULT_OK:
                    $this->_clearQuote($order);
                    $state = $this->getOrderSuccessState($order->getStoreId());
                    $status = $this->getOrderSuccessStatus($order->getStoreId());
                    $isOrderSuccessful = true;
                    break;

                case Webgriffe_BancaSellaGw_Model_PaymentMethod::TRANS_RESULT_KO:
                    $state = $this->getOrderFaultState($order->getStoreId());
                    $status = $this->getOrderFaultStatus($order->getStoreId());
                    if ($this->_isAbandonedTransaction($errorCode)) {
                        $state = $this->getOrderCanceledState($order->getStoreId());
                        $status = $this->getOrderCanceledStatus($order->getStoreId());
                    }
                    break;

                default:
                    $state = $this->getOrderCanceledState($order->getStoreId());
                    $status = $this->getOrderCanceledStatus($order->getStoreId());
                    break;
            }

            if (! $this->_isOrderAlreadyProcessed($order)) {
                $this->_processOrder($order, $state, $status, $thePaymentMethod, $errorCode);
            } else {
                $this->log("Order '#%s' already processed.", $order->getIncrementId());
            }
        } else {
            Mage::throwException(sprintf("Couldn't retrieve Order '#%s'", $order->getIncrementId()));
        }

        return array('success' => $isOrderSuccessful, 'order' => $order);
    }

    /**
     * Azzera il carrello legato all'Ordine
     *
     * @param Mage_Sales_Model_Order $order
     */
    protected function _clearQuote(Mage_Sales_Model_Order $order)
    {
        $quote = Mage::getModel('sales/quote')->load($order->getQuoteId());
        if ($quote->getId()) {
            $quote->setIsActive(false)->save();
        } else {
            Mage::throwException(sprintf("Missing Quote Object '%s' for Order '%s'", $order->getQuoteId(), $order->getId()));
        }
    }

    /**
     * @param Mage_Sales_Model_Order $order
     */
    protected function _isOrderAlreadyProcessed(Mage_Sales_Model_Order $order)
    {
        /** @var Mage_Payment_Model_Method_Abstract $paymentInstance */
        $paymentInstance = $order->getPayment()->getMethodInstance();

        /** @var Mage_Payment_Model_Info $infoInstance */
        $infoInstance = $paymentInstance->getInfoInstance();

        return ($infoInstance->hasAdditionalInformation('processed'));
    }

    /** @return Webgriffe_BancaSellaGw_Model_PaymentMethod */
    protected function _getPaymentModel() {
        return Mage::getModel('bancasellagw/paymentMethod');
    }

    protected function _getConfig($key, $storeId = null) {
        return $this->_getPaymentModel()->getConfigData($key, $storeId);
    }


    /**
     * @param Mage_Sales_Model_Order $order
     * @param string $state
     * @param array $transactionResult
     * @param array $buyer
     */
    protected function _updateOrder(Mage_Sales_Model_Order $order, string $state, $status, array $transactionResult, array $buyer)
    {
        $comment = sprintf(
            "BankTransactionID: %s<br/>AuthorizationCode: %s<br/>BuyerName: %s<br/>BuyerEmail: %s<br/>ErrorCode: %s<br/>ErrorDescription: %s",
            $transactionResult['transaction_id'],
            $transactionResult['auth_code'],
            $buyer['buyer_name'],
            $buyer['buyer_email'],
            $transactionResult['error_code'],
            $transactionResult['error_description']
        );

        if (!strcmp(Mage_Sales_Model_Order::STATE_CANCELED, $state)) {
            $order->cancel()->save();
            $order
                ->addStatusHistoryComment($comment, $status)
                ->save();
            $this->log(
                "Order '#%s' canceled with state '%s' and status '%s'.",
                $order->getIncrementId(),
                $state,
                $status
            );
        } else {
            $order = $order
                ->setState($state, $status, $comment)
                ->save();
            if (! $order->getEmailSent()) {
                $order->sendNewOrderEmail();
                $this->log("Confirmation email sent for Order '#%s'", $order->getIncrementId());
            }
            $this->log(
                "Order '#%s' updated with state '%s' and status '%s'.",
                $order->getIncrementId(),
                $state,
                $status
            );

            if ($order->canInvoice()
                    && $this->getOrderGenerateInvoice($order->getStoreId())) {
                $this->_createInvoice($order);
            }
        }
        $this->_setOrderProcessed($order);
    }

    /**
     * @param Mage_Sales_Model_Order $order
     */
    protected function _createInvoice(Mage_Sales_Model_Order $order)
    {
        // Create 'pending' invoice
        $invoiceId = Mage::getModel('sales/order_invoice_api')
            ->create($order->getIncrementId(), array());

        $invoice = Mage::getModel('sales/order_invoice')
            ->loadByIncrementId($invoiceId);

        // Set Invoice to 'paid'
        $notifyCustomer = $this->getOrderNotifyCustomer($order->getStoreId());
        $invoice->pay()->sendEmail($notifyCustomer)->save();
    }

    /**
     * @param Mage_Sales_Model_Order $order
     */
    protected function _setOrderProcessed(Mage_Sales_Model_Order $order)
    {
        /** @var Mage_Payment_Model_Method_Abstract $paymentInstance */
        $paymentInstance = $order->getPayment()->getMethodInstance();

        /** @var Mage_Payment_Model_Info $infoInstance */
        $infoInstance = $paymentInstance->getInfoInstance();

        $infoInstance->setAdditionalInformation('processed', true)->save();
    }

    protected function _isAbandonedTransaction($code)
    {
        return in_array($code, array('516', '1140', '1143', '1201', '2005'));
    }

    /**
     * @param string $status
     * @return string
     */
    public function loadStateFromStatus($status)
    {
        $state = $status = Mage::getModel('sales/order_status')
            ->getCollection()
            ->joinStates()
            ->addFieldToFilter('main_table.status', array('eq' => $status))
            ->getFirstItem();
        return $state->getState();
    }

    /**
     * @param $order
     * @param $state
     * @param $status
     * @param $thePaymentMethod
     * @param $errorCode
     */
    protected function _processOrder($order, $state, $status, $thePaymentMethod, $errorCode)
    {
        if (!empty($state)) {
            $transactionResult = array(
                'transaction_id' => $thePaymentMethod->bankTransactionId,
                'error_code' => $errorCode,
                'error_description' => $thePaymentMethod->decryptErrorDescription,
                'auth_code' => $thePaymentMethod->authorizationCode);

            $buyer = array(
                'buyer_name' => $thePaymentMethod->buyerName,
                'buyer_email' => $thePaymentMethod->buyerEmail);

            $this->_updateOrder($order, $state, $status, $transactionResult, $buyer);
            $this->log("Order '#%s' processed.", $order->getIncrementId());
        } else {
            Mage::throwException("Order Success State was not configured properly");
        }
    }

}