<?php
class Webgriffe_BancaSellaGw_Model_Currency
{
    public function toOptionArray()
    {
	    return array(
	      array('value' => '242', 'label' => Mage::helper('bancasellagw')->__('EUR'))
	    );
    }

} 