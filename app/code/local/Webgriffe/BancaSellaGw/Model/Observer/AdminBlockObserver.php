<?php
class Webgriffe_BancaSellaGw_Model_Observer_AdminBlockObserver
{
	public function addButton($observer)
	{
		$block = $observer->getBlock();
		if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View)
		{
			// Get the order_id
			$orderId = $block->getRequest()->getParam('order_id');
			
			// Get the order object
			$order = Mage::getSingleton('sales/order');
			$order->load($orderId);
			
			// Get Payment Method
			$paymentMethod = ($order->getPayment() ? $order->getPayment()->getMethod() : '');	    
	
			// Get the order state
			if($order->getState() == Mage_Sales_Model_Order::STATE_PENDING_PAYMENT
				&& !strcasecmp($paymentMethod, 'bancasellagw'))
			{
				$block->addButton(
			    	'order_newaction', 
					array(
						'label' => Mage::helper('core')->__('Repeat payment'),
						'onclick' => 'setLocation(\'' . $block->getUrl('bancasellagw/return/adminRepay') . '\')',
					)
				);
			}
		}
	}
}