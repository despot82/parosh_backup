<?php

class Webgriffe_BancaSellaGw_Model_PaymentMethod extends Mage_Payment_Model_Method_Abstract {
    //-----------------------------------
    // Constants
    //-----------------------------------

    const METHOD_CODE = 'bancasellagw';

    const PAYMENT_TYPE_AUTH = 'AUTHORIZATION';
    const PAYMENT_TYPE_SALE = 'SALE';

    const TRANS_RESULT_OK   = 'OK'; // Esito transazione positivo
    const TRANS_RESULT_KO   = 'KO'; // Esito transazione negativo
    const TRANS_RESULT_XX   = 'XX'; // Esito transazione sospeso (solo bonifico)

    protected $_languages = array(
        'it' => '1',
        'en' => '2',
        'es' => '3',
        'fr' => '4',
        'de' => '5',
    );

    protected $_helper = null;

    //-----------------------------------
    // Properties
    //-----------------------------------

    protected $_code = self::METHOD_CODE;
    protected $_responseUrl = 'bancasellagw/return/response';
    protected $_errorUrl = 'bancasellagw/return/error';
    public $cryptResponse = false;
    public $cryptedString = "";
    public $encryptErrorCode = "";
    public $encryptErrorDescription = "";
    public $decryptedResponse = "KO";
    public $decryptedOrderId = "-1";
    public $decryptErrorCode = "";
    public $decryptErrorDescription = "";

    //-----------------------------------
    // Methods
    //-----------------------------------

    public function __construct() {
        parent::__construct();
        $this->_helper = Mage::helper('bancasellagw');
    }

    /**
     * Get checkout session namespace
     *
     * @return Mage_Checkout_Model_Session
     */
    public function getCheckout() {
        return Mage::getSingleton('checkout/session');
    }

    /**
     * Get current quote
     *
     * @return Mage_Sales_Model_Quote
     */
    public function getQuote() {
        return $this->getCheckout()->getQuote();
    }

    /**
     * Using internal pages for input payment data
     *
     * @return bool
     */
    public function canUseInternal() {
        return true;
    }

    /**
     * Using for multiple shipping address
     *
     * @return bool
     */
    public function canUseForMultishipping() {
        return false;
    }

    public function isInitializeNeeded() {
        return true;
    }

    /*
     * -----------------------------------
     * function validate()
     * -----------------------------------
     */

    public function validate() {

        $this->_helper->log('BancaSellaGw validate');

        $order_new = $this->getConfigData('order_new_state');
        $order_fault = $this->getConfigData('order_fault_state');
        $order_canceled = $this->getConfigData('order_canceled_state');
        $order_success = $this->getConfigData('order_success_state');
        $url = trim($this->getConfigData('payment_url'));
        $cryptDecryptUrl = trim($this->getConfigData('cryptdecrypt_service_url'));
        $userID = $this->getConfigData('shop_login');


        $valuta = $this->getConfigData('valuta');


        if (empty($url) || empty($order_new) || empty($order_canceled)
                || empty($order_fault) || empty($order_success) || empty($cryptDecryptUrl)
                || empty($userID) || empty($valuta)) {
            Mage::throwException(Mage::helper('bancasellagw')
                            ->__('Wrong or missing configuration parameters for Banca Sella payment method.'));
        }

        return $this;
    }

    /*
     * -----------------------------------
     * function initialize()
     * -----------------------------------
     */

    public function initialize($paymentAction, $stateObject) {
        $this->_helper->log('Banca Sella Gw initialize');

        // Get configuration parameters
        $order_new = $this->getConfigData('order_new_state');
        $stateObject->setState($order_new);
        $stateObject->setStatus($order_new);
        $stateObject->setIsNotified(false);
        $url = trim($this->getConfigData('payment_url'));

        $userID = $this->getConfigData('shop_login');
        $valuta = $this->getConfigData('valuta');

        //Order number (used as Transaction ID)
        $numeroOrdine = $this->getQuote()->getReservedOrderId();

        // Address (used to determine amount)
        if ($this->getQuote()->isVirtual()) {
            $address = $this->getQuote()->getBillingAddress();
        } else {
            $address = $this->getQuote()->getShippingAddress();
        }


        // Amount
        $amount = $address->getBaseGrandTotal();

        //Check if debug mode is set: amount must be 0.01
        $bDebugMode = $this->getConfigData('debug_mode');
        if ($bDebugMode == 1) {
            $amount = "0.01";
        }

        //Round the amount
        $amount = round($amount, 2);

        // Customer e-mail
        /** @var Mage_Customer_Model_Customer $customer */
        $customer = $this->getQuote()->getCustomer();
        $customerEmail = $customer->getEmail();
        $customerName = $customer->getName();

        // Language id			
        $lang = $this->getQuote()->getStore()->getConfig('general/locale/code');
        $lang = substr($lang, 0, 2);

        $this->_helper->log('lang key: %s', $lang);
        if (array_key_exists($lang, $this->_languages)) {
            $lang = $this->_languages[$lang];
        } else {
            $lang = $this->_languages['it'];
        }
        $this->_helper->log('lang code: %s', $lang);

        //Params
        $theShopLogin = $userID;
        $shopTransactionId = $numeroOrdine;
        $amount = $amount;
        $uicCode = $this->getConfigData('valuta');
        //$uicCode=$amount;
        //Crypt the params
        $cryptParams = array(
            'shopLogin' => $theShopLogin,
            'uicCode' => $uicCode,
            'amount' => $amount,
            'shopTransactionId' => $shopTransactionId,
            // campi aggiunti in data 29/08/2013
            #'buyerName' => $customerName,
            #'buyerEmail' => $customerEmail,
            #'languageId' => $lang,
            // customInfo deve essere usato solo in presenza di parametri
            // aggiuntivi configurati nel backend di Banca Sella
            #'customInfo' => sprintf("SessionId=%s*P1*SiteLanguage=%s", $shopTransactionId, $lang),
        );

        $this->_helper->log("CryptParams are listed below.\r\n%s", print_r($cryptParams, TRUE));

        if (function_exists('xdebug_disable')) {
            xdebug_disable();
        }
        try {
            //Encrypt
            $this->crypt($cryptParams);
        } catch (Exception $e) {
            Mage::getSingleton('bancasellagw/session')->setInitializeError(true);
            $this->_helper->log('Exception: %e', $e->getMessage());
            return $this;
        }
        if (function_exists('xdebug_enable')) {
            xdebug_enable();
        }

        //Check the transaction result
        if (!($this->cryptResponse))
        {
            $this->_helper->log('Initialize error');
            $this->_helper->log(
                'ERR CODE: %s - ERR DESCR: %s',
                $this->encryptErrorCode,
                $this->encryptErrorDescription
            );

            //Redirect to the failure page
            //Mage::_redirect($this->_errorUrl);
            Mage::getSingleton('bancasellagw/session')->setInitializeError(true);
            return $this;
        }

        $params = array(
            'shopLogin' => $theShopLogin,
            'cryptString' => $this->cryptedString
        );

        $this->_helper->log(print_r($params, TRUE));

        Mage::getSingleton('bancasellagw/session')->setPaymentUrl($url);
        Mage::getSingleton('bancasellagw/session')->setPaymentParams($params);

        return $this;
    }

    /*
     * -----------------------------------
     * function getOrderPlaceRedirectUrl()
     * -----------------------------------
     */

    public function getOrderPlaceRedirectUrl() {
        if (Mage::getSingleton('bancasellagw/session')->getInitializeError() === true) {
            Mage::getSingleton('bancasellagw/session')->unsInitializeError();
            return Mage::getUrl('checkout/onepage/failure');
        }

        return Mage::getUrl('bancasellagw/return/bankpost');
    }

    /*
     * -----------------------------------
     * function crypt()
     * -----------------------------------
     */

    public function crypt($cryptParams) {

        //Reset crypt/decrypt props
        $this->cryptResponse = false;
        $this->cryptedString = "";
        $this->encryptErrorCode = "";
        $this->encryptErrorDescription = "";

        //Get encryption service URL
        $sWSCryptDecrypUrl = trim($this->getConfigData('cryptdecrypt_service_url'));
        $this->_helper->log('WSDL URL: %s', $sWSCryptDecrypUrl);

        $client = new SoapClient($sWSCryptDecrypUrl, array('exceptions' => true));

        //Call the encryption service
        $result = $client->Encrypt($cryptParams);
        $this->_helper->log(print_r($result, true));

        //Get the encryption result
        $resp = $result->EncryptResult;
        $theXmlResponse = $resp->any;
        $this->_helper->log('crypt XML response: %s', $theXmlResponse);

        //Create the DOMDocument
        $objDOM = new DOMDocument();
        $objDOM->loadXml($theXmlResponse);

        //TransactionResult
        $oTransactionResult = $objDOM->getElementsByTagName("TransactionResult");
        $sTransactionResult = $oTransactionResult->item(0)->nodeValue;

        //Check the transaction result
        if (stristr($sTransactionResult, "OK")) {

            //Success
            $this->_helper->log('SOAP CALL OK');
            $this->cryptResponse = true;

            //CryptDecryptString
            $oCryptString = $objDOM->getElementsByTagName("CryptDecryptString");
            $sCryptString = $oCryptString->item(0)->nodeValue;

            $this->cryptedString = $sCryptString;
        } elseif (stristr($sTransactionResult, "KO")) {

            //Error
            $this->_helper->log('Crypt SOAP CALL FAIL');

            //Set the error properties
            //ErrorCode
            $oCryptString = $objDOM->getElementsByTagName("ErrorCode");
            $sCryptString = $oCryptString->item(0)->nodeValue;
            $this->encryptErrorCode = $sCryptString;

            //ErrorDescription
            $oCryptString = $objDOM->getElementsByTagName("ErrorDescription");
            $sCryptString = $oCryptString->item(0)->nodeValue;
            $this->encryptErrorDescription = $sCryptString;

            $this->_helper->log(
                'ERR CODE: %s - ERR DESCR: %s',
                $this->encryptErrorCode,
                $this->encryptErrorDescription
            );
        }
    }

    /*
     * -----------------------------------
     * function decrypt()
     * -----------------------------------
     */

    public function decrypt($decryptParams) {

        //Reset crypt/decrypt props
        $this->decryptedResponse = "KO";
        $this->decryptedOrderId = "-1";

        //Get encryption service URL
        $sWSCryptDecrypUrl = trim($this->getConfigData('cryptdecrypt_service_url'));
        $client = new SoapClient($sWSCryptDecrypUrl);

        //Call the encryption service
        $result = $client->Decrypt($decryptParams);

        //Get the encryption result
        $resp = $result->DecryptResult;
        $theXmlResponse = $resp->any;
        $this->_helper->log('decrypt XML response: %s', $theXmlResponse);

        //Create the DOMDocument
        $objDOM = new DOMDocument();
        $objDOM->loadXml($theXmlResponse);

        //TransactionResult
        $oTransactionResult = $objDOM->getElementsByTagName("TransactionResult");
        $sTransactionResult = $oTransactionResult->item(0)->nodeValue;

        //ShopTransactionID (Order #)
        $oTransactionId = $objDOM->getElementsByTagName("ShopTransactionID");
        $this->decryptedOrderId = $oTransactionId->item(0)->nodeValue;

        // ============================================= Additional Params BEGIN

        //BankTransactionID
        $oBankTransactionId = $objDOM->getElementsByTagName("BankTransactionID");
        $this->bankTransactionId = $oBankTransactionId->item(0)->nodeValue;

        //AuthorizationCode
        $oAuthorizationCode = $objDOM->getElementsByTagName("AuthorizationCode");
        $this->authorizationCode = $oAuthorizationCode->item(0)->nodeValue;

        //BuyerName
        $oBuyerName = $objDOM->getElementsByTagName("BuyerName");
        $this->buyerName = $oBuyerName->item(0)->nodeValue;

        //BuyerEmail
        $oBuyerEmail = $objDOM->getElementsByTagName("BuyerEmail");
        $this->buyerEmail = $oBuyerEmail->item(0)->nodeValue;

        // =============================================== Additional Params END

        //Check the transaction result
        if (stristr($sTransactionResult, "OK")) {

            //Success

            $this->decryptedResponse = "OK";
        } elseif (stristr($sTransactionResult, "KO")) {

            //Error
            $this->_helper->log('Decrypt SOAP CALL FAIL');

            //Set the error properties
            //ErrorCode
            $oCryptString = $objDOM->getElementsByTagName("ErrorCode");
            $sCryptString = $oCryptString->item(0)->nodeValue;
            $this->decryptErrorCode = $sCryptString;

            //ErrorDescription
            $oCryptString = $objDOM->getElementsByTagName("ErrorDescription");
            $sCryptString = $oCryptString->item(0)->nodeValue;
            $this->decryptErrorDescription = $sCryptString;

            $this->_helper->log(
                'ERR CODE: %s - ERR DESCR: %s',
                $this->decryptErrorCode,
                $this->decryptErrorDescription
            );
        }
    }

}