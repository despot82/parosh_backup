<?php
/**
 * Order Statuses source model for Unicredit
 */
class Webgriffe_BancaSellaGw_Model_System_Config_Source_Order_Status{

    // set null to enable all possible
    protected $_stateStates = array(
		Mage_Sales_Model_Order::STATE_NEW,
		Mage_Sales_Model_Order::STATE_PENDING_PAYMENT,
        Mage_Sales_Model_Order::STATE_PROCESSING,
        Mage_Sales_Model_Order::STATE_CANCELED,
        Mage_Sales_Model_Order::STATE_HOLDED,
    );

    public function toOptionArray()
    {
        if ($this->_stateStates) {
            $statuses = Mage::getModel('sales/order_config')->getStateStatuses($this->_stateStates);
        }
        else {
            $statuses = Mage::getModel('sales/order_config')->getStatuses();
        }
		
        $options = array();
        $options[] = array(
               'value' => '',
               'label' => Mage::helper('adminhtml')->__('-- Please Select --')
            );
        foreach ($statuses as $code=>$label) {
            $options[] = array(
               'value' => $code,
               'label' => $label
            );
        }
        return $options;
    }
}