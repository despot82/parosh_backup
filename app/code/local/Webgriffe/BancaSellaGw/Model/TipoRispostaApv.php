<?php
class Webgriffe_BancaSellaGw_Model_TipoRispostaApv
{
    public function toOptionArray()
    {
	    return array(
	      array('value' => 'wait', 'label' => Mage::helper('bancasellagw')->__('wait')),
	      array('value' => 'click', 'label' => Mage::helper('bancasellagw')->__('click'))
	    );
    }

} 