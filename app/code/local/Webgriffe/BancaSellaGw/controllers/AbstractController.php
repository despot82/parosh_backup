<?php
abstract class Webgriffe_BancaSellaGw_AbstractController extends Mage_Core_Controller_Front_Action
{
    /** @var Webgriffe_BancaSellaGw_Helper_Data  */
    protected $_helper = null;

    protected function _construct() {
        $this->_helper = Mage::helper('bancasellagw');
    }

}