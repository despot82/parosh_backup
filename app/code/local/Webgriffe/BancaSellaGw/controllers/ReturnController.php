<?php
require_once 'Webgriffe/BancaSellaGw/controllers/AbstractController.php';
class Webgriffe_BancaSellaGw_ReturnController extends Webgriffe_BancaSellaGw_AbstractController
{

    //-----------------------------------
    // Methods: actions
    //-----------------------------------

    /**
     * -----------------------------------
     * function bankpostAction()
     * -----------------------------------
     * Triggered by the Place Order action
     * Called by Magento to redirect to Payment Gateway Page
     */
    public function bankpostAction()
    {

        Mage::app();

        $params = $this->_helper->getSession()->getPaymentParams();

        $quoteId = Mage::getSingleton('checkout/session')->getLastSuccessQuoteId();
        $quote = Mage::getModel('sales/quote')->load($quoteId);
        if ($quote->getId()) {
            $quote->setIsActive(false)->save();
        }

        // Unset session data which is no more used
        $this->_helper->getSession()->unsPaymentUrl();
        if (!is_array($params)) {
            return;
        }

        //Send payment data to the gateway
        $this->_sendPaymentData($params);
    }

    /**
     * Called by the Gateway on completion of payment procedure.
     * Payment procedure can be successful or unsuccessful.
     * 
     * This action is triggered by /bancasellagw/return/responseOk
     * Return from: OK
     */
    public function responseOkAction()
    {
        /** @var array $result */
        $result = $this->_helper->processResponse($this->getRequest());

        /** @var boolean $success */
        $success = $result['success'];

        /** @var Mage_Sales_Model_Order $order */
        $order = $result['order'];

        // In caso di chiamata a OkAction con Pagamento NON a buon fine
        // lo gestiamo come fosse una KoAction
        if (! $success) {
            return $this->responseKoAction($order);
        }

        //
        // Redirect (si assume pagamento a buon fine)
        //
        $orderNumber = $order->getIncrementId();
        $sRedirectUrl = $this->_helper->getSession()->getReturnPage();
        if ($sRedirectUrl != null && $sRedirectUrl != '' && stripos($sRedirectUrl, "http:") >= 0) {
            //...From repeat payment action and URL is absolute
            $this->_helper->log('Repeat payment #%s successful', $orderNumber);
            $this->_redirectUrl($sRedirectUrl);
        } elseif ($sRedirectUrl != null && $sRedirectUrl != '') {
            //...From repeat payment action
            $this->_helper('Repeat payment #%s successful', $orderNumber);
            $store = $order->getStore();
            $this->storeRedirect($store, $sRedirectUrl);
        } else {
            //...From normal payment action
            $this->_helper->log('Payment successful for Order #%s', $orderNumber);
            $store = $order->getStore();
            $this->storeRedirect($store, 'checkout/onepage/success');
        }
    }

    /**
     * Called by the Gateway on completion of payment procedure.
     * Payment procedure can be successful or unsuccessful.
     * 
     * This action is triggered by /bancasellagw/return/responseKo
     * Return from: KO or Cancel
     */
    public function responseKoAction($order = null)
    {
        /** @var Mage_Sales_Model_Order $order */
        if (is_null($order)) {
            /** @var array $result */
            $result = $this->_helper->processResponse($this->getRequest());

            /** @var Mage_Sales_Model_Order $order */
            $order = $result['order'];
        }

        //
        // Redirect (si assume pagamento NON a buon fine)
        //
        $this->_helper->log('Payment unsuccessful for Order #%s', $order->getIncrementId());

        $store = $order->getStore();
        $this->storeRedirect($store, 'checkout/onepage/failure');
    }

    //====== REPEAT PAYMENT ======

    /**
     * -----------------------------------
     * function adminRepayAction()
     * -----------------------------------
     * Repeat Payment for pending orders
     */
    public function adminRepayAction()
    {

        //=====Get the order_id
        $orderId = $this->getRequest()->getParam('order_id');

        //Set the success return page
        $this->_helper->getSession()->setReturnPage(Mage::getUrl('adminhtml/sales_order/view/order_id/' . $orderId));

        $this->doRepay($orderId);
    }

    /**
     * -----------------------------------
     * function repayAction()
     * -----------------------------------
     * Repeat Payment for pending orders
     */
    public function repayAction()
    {
        //=====Get the order_id
        $orderId = $this->getRequest()->getParam('order_id');

        $order = Mage::getModel('sales/order')->load($orderId);
        if ($order->getId()) {
            if ($order->getState() == Mage::getModel('bancasellagw/paymentMethod')->getConfigData('order_new_state')) {
                //$this->_redirect('*/*/redirect', array('_secure' => true, 'par'=>base64_encode($params['id'] ) ) ) ;
                //Set the success return page
                $this->_helper->getSession()->setReturnPage('sales/order/view/order_id/' . $orderId);
                $this->doRepay($orderId);
            } else {
                $this->_redirect('sales/order/view', array('_secure' => true, 'order_id' => $orderId));
            }
        }
    }

    /**
     * -----------------------------------
     * function doRepay()
     * -----------------------------------
     * Repeat Payment for pending orders
     */
    public function doRepay($orderId)
    {

        //Get the Payment Method
        $thePaymentMethod = Mage::getModel('bancasellagw/paymentMethod');

        //Get the order object
        $order = Mage::getSingleton('sales/order');
        $order->load($orderId);

        $numeroOrdine = $order->getIncrementId();

        // Amount
        $amount = $order->getBaseGrandTotal();
        $storeCurrency = Mage::getSingleton('directory/currency')
                ->load($order->getBaseCurrencyCode());
        $amount = $storeCurrency->convert($amount, 'EUR');
        //Check if debug mode is set: amount must be 0.01
        $bDebugMode = $thePaymentMethod->getConfigData('debug_mode');
        if ($bDebugMode == 1) {
            $amount = "0.01";
        }

        //Round the amount
        $amount = round($amount, 2);

        //Get payment params
        $theShopLogin = $thePaymentMethod->getConfigData('shop_login');
        $uicCode = $thePaymentMethod->getConfigData('valuta');


        //=====Re-send payment request
        //Crypt the params
        $cryptParams = array(
            'shopLogin' => $theShopLogin,
            'uicCode' => $uicCode,
            'amount' => $amount,
            'shopTransactionId' => $numeroOrdine,
        );

        if (!is_array($cryptParams)) {
            return;
        }

        //Encrypt
        $thePaymentMethod->crypt($cryptParams);

        //Check if the crypt method is successful
        if (!($thePaymentMethod->cryptResponse)) {

            //Error
            $this->_helper->log('Initialize error');
            $this->_helper->log(
                'ERR CODE: %s - ERR DESCR: %s',
                $thePaymentMethod->encryptErrorCode,
                $thePaymentMethod->encryptErrorDescription
            );

            $this->_redirect('checkout/onepage/failure');
            return;
        }

        $paymentParams = array(
            'shopLogin' => $theShopLogin,
            'cryptString' => $thePaymentMethod->cryptedString
        );

        //Send payment data to the gateway
        $this->_sendPaymentData($paymentParams);
    }

    /**
     * -----------------------------------
     * function _sendDataPage()
     * -----------------------------------
     * Send payment data to the gateway
     */
    protected function _sendPaymentData($params)
    {

        //Get payment url
        $url = $this->_helper->getSession()->getPaymentUrl();

        $this->_helper->log(
            "Parameters that will be sent are listed below.\r\n%s",
            print_r($params, true)
        );

        //Params
        $theShopLogin = $params['shopLogin'];
        $sCryptString = $params['cryptString'];

        $theRedirectUrl = Mage::getModel('bancasellagw/paymentMethod')->getConfigData('payment_url');

        $this->_redirectUrl($theRedirectUrl . "?a=" . $theShopLogin . "&b=" . $sCryptString);
    }

    public function storeRedirect($store, $redirectPath)
    {
        $this->_redirectUrl($store->getUrl($redirectPath));
    }

}