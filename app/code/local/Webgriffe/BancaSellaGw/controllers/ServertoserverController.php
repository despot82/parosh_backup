<?php
require_once 'Webgriffe/BancaSellaGw/controllers/AbstractController.php';
class Webgriffe_BancaSellaGw_ServertoserverController extends Webgriffe_BancaSellaGw_AbstractController
{

	//-----------------------------------
	// Methods: actions
	//-----------------------------------
	
	/**
	 * -----------------------------------
	 * function updateOrderAction()
	 * -----------------------------------
	 * Server to server communication
	 * 
	 * Called by the Gateway on completion of payment procedure.
	 * Payment procedure can be successful or unsuccessful.
	 */
	public function updateOrderAction()
    {
        $this->_helper->log("Got Server To Server Call");

        /** @var array $result */
        $result = $this->_helper->processResponse($this->getRequest());
    }
	
}