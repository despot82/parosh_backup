<?php

define('MAGE_ROOT', realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../../..'));
require_once(MAGE_ROOT . '/app/Mage.php');

Mage::app()->setCurrentStore(0);


Mage::getModel('catalogrule/rule')->applyAll();
Mage::getModel('catalogrule/flag')->loadSelf()
        ->setState(0)
        ->save();
?>