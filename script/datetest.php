<?php

define('MAGE_ROOT', realpath(dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../../..'));
require_once(MAGE_ROOT . '/app/Mage.php');

Mage::app()->setCurrentStore(0);

echo "Date Locale Default: " . date("Y-m-d H:i:s") . "\n";
echo "Date Magento Admin: " . Mage::getModel('core/date')->date("Y-m-d H:i:s") . "\n";
