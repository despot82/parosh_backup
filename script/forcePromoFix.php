<?php
/**
 * Questo script forza l'esecuzione del fix delle promo.
 * Il fix è settato in cron per girare ogni notte alle 24
 */

//Init
error_reporting(E_ALL);
ini_set('display_errors', '1');

require('../app/Mage.php');
Mage::app()->setCurrentStore(0);

$fixer = Mage::getModel('promofix/default');
$fixer->apply();