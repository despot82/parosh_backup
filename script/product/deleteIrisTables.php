<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

require('../../app/Mage.php');
Mage::app()->setCurrentStore(0);
Mage::setIsDeveloperMode(true);

$tables = array(
        'iris_product_action',
        'iris_product_action_attribute',
//        'iris_product_action_link'
);

$write = Mage::getSingleton('core/resource')->getConnection('core_write');
foreach($tables as $table){
    $query = "DELETE FROM {$table} WHERE 1";
    $write->query($query);
    echo "Tabella: ".$table." svuotata";
}

