<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

require('../../app/Mage.php');
Mage::app()->setCurrentStore(0);
Mage::setIsDeveloperMode(true);


$products = Mage::getModel('catalog/product')->getCollection();    
foreach ($products as $product) {
    try {
        $product->delete();
    } catch(Exception $e) {
        echo "Product #".$product->getId()." could not be remvoved: ".$e->getMessage();
    }
}

?>

