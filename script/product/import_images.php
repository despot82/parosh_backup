<?php

$mageFilename = '../../app/Mage.php';
require_once $mageFilename;
Mage::setIsDeveloperMode(true);
ini_set('display_errors', 1);
ini_set('max_execution_time', 0);

ini_set('memory_limit', '2048M');  
umask(0);


Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$handle = fopen(Mage::getBaseDir('base').'/script/files/prodotti_pe2014.csv', 'r');
$imageDir = Mage::getBaseDir('base').'/script/files/immagini/';


$arrayProdotti = array();

$mediaApi = Mage::getModel("catalog/product_attribute_media_api");

while (($data = fgetcsv($handle, 0, '#', '"')) !== false) 
{


    //$data[8] = preg_replace('/[\r\n]+/', ' ', $data[8]);  
    $prodotto = array(
        "sku" => substr(strtolower(trim($data[0])),0,8)."_conf",
        "image1" => trim($data[13]),
        "image2" => trim($data[14]),
        "image3" => trim($data[15]),
        "image4" => trim($data[16]),
        "image5" => trim($data[17]),        
    );
    
    if(trim($prodotto["sku"]) == ""){
        continue;
    }

    $arrayProdotti[substr(strtolower(trim($data[0])),0,8)."_conf"] = $prodotto;
    
}



$count = 0;
$imageCount = 0;
foreach ($arrayProdotti as $product) {
//    if($imageCount == 5){
//        die();
//    }

    //Mage::helper("iris/log")->log($prodotto["sku"]." - ".$prodotto["image"]);

    try {

        
        
        if ($product["sku"]) {

            //carico by sku
            $model = Mage::getModel("catalog/product");
        

            $productId = $model->getIdBySku($product["sku"]);


            if (!$productId) {
                continue;
            }
        
            $product = $model->load($productId);

            
            
            //rimuovo immagini dai prodotti
            
            //Mage::helper("iris/log")->log("Check image for product " . $product->getSku());
            
            
            //rimuovo immagini dai prodotti
            $mediaApi = Mage::getModel("catalog/product_attribute_media_api");
            $items = $mediaApi->items($product->getId());
        
            if(count($items) > 0){
                foreach ($items as $item) {
                    $mediaApi->remove($product->getId(), $item['file']);
                    unlink(Mage::getBaseDir() . DS . 'media' . DS . 'catalog' . DS . 'product' . $item['file']);
                }
                unset($mediaApi);

                echo "Removed images on product ".$product->getId() . "\n";
                $product = $product->load($product->getId());
            }         
            
            
            
            
            
            
            //salvo l'immgine  

            
            
            
           
            Mage::helper("iris/log")->log("Saving product ".$product->getName());
            $found = false;
            foreach ($arrayProdotti[strtolower(trim($product->getSku()))] as $key => $value) {
                if($key == "sku"){
                    continue;
                }
                $value = $value.".jpg";
                
                
                Mage::helper("iris/log")->log("image ".$value);
                if($value == "" || !file_exists($imageDir .$value )){  
                    //controllo che le immagini esistano
                    
                    
                    //controllo che l'immagine non si chiami in modo diverso
                    $imageExplode = explode("_", $value);
                    
                    
                    if(strpos($imageExplode[2], "0") === 0){
                        //se inizia con zero lo tolgo
                        $value = $imageExplode[0]."_".$imageExplode[1]."_".trim($imageExplode[2],"0");
                    }else{
                        //aggiungo lo zero
                        $value = $imageExplode[0]."_".$imageExplode[1]."_"."0".$imageExplode[2];
                    }
                    
                   
                    if($value == "" || !file_exists($imageDir .$value )){  
                        Mage::helper("iris/log")->log("image not exist ".$value);                    
                        continue;                        
                    }

                }
                
                $found = true;
                if($key == "image1"){
                    $visibility = array(
                        'thumbnail',
                        'small_image',
                        'image'
                    );
                }else{
                    $visibility = array(
                    );
                }
                $product->addImageToMediaGallery($imageDir . $value, $visibility, false, false);
                
                
                Mage::helper("iris/log")->log("import product image:".$value." for product ".$product->getId());                
            }
            if($found){
                $product->save();
            }
            Mage::helper("iris/log")->log("");
            $imageCount++;

        }
        
    } catch (Exception $exc) {
        Mage::helper("iris/log")->log($exc->getMessage());
    }
    
    
}

?>
