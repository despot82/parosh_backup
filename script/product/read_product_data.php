<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

require('../../app/Mage.php');
Mage::app()->setCurrentStore(0);
Mage::setIsDeveloperMode(true);


//INIZIO ESEMPIO LETTURA DATI
//creo il parser di tipo csv

//Davide 10/07/2014
//$csv_parser = FiloBlu_Iris_Model_Parser_Product_Csv_Default::fromLocalFile(Mage::getBaseDir('base').'/script/files/prodotti_pe2014.csv');

if(!isset($argv[1])) {
    die("Please specify the filename of the CSV in /[BASE DIR]/scripts/files/ as a parameter");
}
$csv_parser = FiloBlu_Iris_Model_Parser_Product_Csv_Default::fromLocalFile(Mage::getBaseDir('base').'/script/files/'.$argv[1]);
//$csv_parser = FiloBlu_Iris_Model_Parser_Product_Csv_Default::fromLocalFile(Mage::getBaseDir('base').'/script/files/ss15_def.csv');
//END Davide 10/07/2014

//ottengo la lista di oggetti da passare al DAL
$data = $csv_parser->readData();

//creo il dal e gli passo gli oggetti
$dalObject = new FiloBlu_Iris_Model_Dal_Product_Default();



foreach ($data as $product) {
    $actionId = $dalObject->insertData($product);
    //$consumer->consumeSimpleProductRow($actionId);
}

//$consumer = new FiloBlu_Iris_Model_Consumer_Default();

//FINE ESEMPIO LETTURA DATI
//$dalObject->cleanAction();
//$consumer = new FiloBlu_Iris_Model_Consumer_Default();
//$consumer->consumeSimpleProductRow(1);

