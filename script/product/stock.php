<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

require('../../app/Mage.php');
Mage::app()->setCurrentStore(0);
Mage::setIsDeveloperMode(true);

// Enrico R. aggiungo questo piccolo while dove setto le quantità

if(!isset($argv[1])) {
    die("Please specify the filename of the CSV in /[BASE DIR]/scripts/files/ as a parameter");
}

//Mage::helper("iris/inventory")->updateProductInventory(3170, 111);
$csv_parser = Mage::getBaseDir('base') . '/script/files/' . $argv[1];

$handleColori = fopen($csv_parser, 'r');
while (($data = fgetcsv($handleColori, 0, '#', '"')) !== false) {
    
    $qty = ((int)trim($data[17]));
    $product_id = 0;
    
    if ($qty > 0) {

       $product_id = Mage::getModel("catalog/product")->getIdBySku( $data[0] );
       
       Mage::helper("iris/inventory")->updateProductInventory($product_id, $qty);
        
    }
    
}

echo "Fine\n";
?>
