<?php

$mageFilename = '../../app/Mage.php';
require_once $mageFilename;
Mage::setIsDeveloperMode(true);
ini_set('display_errors', 1);
ini_set('max_execution_time', 0);

ini_set('memory_limit', '2048M');  
umask(0);

Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$handle = fopen(Mage::getBaseDir('base').'/script/files/IMPORT_COLORI.csv', 'r');

$arrayProdotti = array();

while (($data = fgetcsv($handle, 0, ',')) !== false) 
{
    $sku = $data[0];
    $color = ucfirst(strtolower($data[1]));
    
    $pId = Mage::getModel("catalog/product")->getIdBySku(trim($sku));
    if($pId){
        $rightAttributeValue = Mage::helper("iris/attribute")->getAttributeOptionValue("color",$color);
        
        if(trim($rightAttributeValue) != ""){
            Mage::log("Updating product ".$pId." with color ".$color . ": ".$rightAttributeValue);
            Mage::getSingleton('catalog/product_action')->updateAttributes(array($pId), array("color" => $rightAttributeValue), 0);
        }
        
    }
}

?>
