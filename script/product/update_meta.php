<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');
ini_set("memory_limit","4096M");
ini_set('max_execution_time', 0);

require('../../app/Mage.php');
Mage::app()->setCurrentStore(0);
Mage::setIsDeveloperMode(true);


$collection = Mage::getModel("catalog/product")->getCollection()->addAttributeToSelect('*');


foreach ($collection as $product) {
    //IT
    $productId = $product->getId();
    $storeId = 1;
    Mage::helper("iris/seo")->generateMetaForProductId($productId, $storeId);
    
    //EN
    $storeId = 2;
    Mage::helper("iris/seo")->generateMetaForProductId($productId, $storeId);
   
}



?>
