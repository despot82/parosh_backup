jq('document').ready(function () {
	jq('body.customer-account-login div.col-main div.account-login:last-of-type').addClass('last');

	var dds = jq('body.catalog-product-view div.col-main div.product-view div.product-essential div.product-data div#product-options-wrapper dl dd');
	dds.each(function (index, dd) {
		var radioButtons = jq(dd).find('input[type=radio]');
		var labels = jq(dd).find('label');
		if (radioButtons.length != labels.length)
			return;

		labels.on('click', function () {
			radioButtons[labels.index(this)].checked = true;

			jq(dd)
				.find('input[type=radio]:not(:checked) + label')
					.removeClass('selected');
			jq(dd)
				.find('input[type=radio]:checked + label')
					.addClass('selected');
		});
	});
});