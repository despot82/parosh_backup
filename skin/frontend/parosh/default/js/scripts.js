jq = jQuery.noConflict(true);

jq('document').ready(function () {
    
    if(jq('#help-top-link').length){
        jq('#help-top-link').appendTo(jq('.header ul.links'))
    }
    
    // cookie consent overlay
    var cookieOverlay = new Overlay('', 'div#cookie-consent-container', 'a.button-close-cookie');

    // cart popup
    new TopLinkPopup('div.header a.top-link-cart', 'div.header div.block-cart');

    // newsletter popup
    new TopLinkPopup('div.header a.top-link-subscribe', 'div.header div.block-subscribe');

    // size chart footer link overlay
    new Overlay('a#size-chart-footer-link', 'div#size-chart-container', 'span.overlay-close-button');

    jq('.header-container li.current-lang').insertAfter('li#cart-top-link');
    jq('.header-container li.switch-lang a').appendTo(jq('li.current-lang'));

    // cms-index-index
    if (jq('body.cms-index-index').length == 1)
    {
        var container = jq('div.col-main div.std > div:last-child');
        if (container.length == 1)
        {
            var boxes = container.find('div.widget-box');
            if (boxes.length > 0)
            {
                container
                        .append('<div id="widget-box-container"></div>')
                        .find('div#widget-box-container')
                        .append(boxes)
                        .masonry({
                            columnWidth: Math.floor(container.width() / 3),
                            itemSelector: 'div.widget-box'
                        });
            }
        }

        var sliderWrapper = jq('div#widget-box-container div.widget-box-slider div.slider-wrapper');
        if (sliderWrapper.length == 1)
        {
            sliderWrapper
                    .find('div.slider')
                    .jcarousel({wrap: 'circular'})
                    .jcarouselAutoscroll({
                        interval: 5000,
                        target: '+=1',
                        autostart: true
                    });
            sliderWrapper.find('a.slider-control-previous').jcarouselControl({target: '-=1'});
            sliderWrapper.find('a.slider-control-next').jcarouselControl({target: '+=1'});
        }
    }

    // catalog-product-view
    if (jq('body.catalog-product-view').length == 1)
    {
        // product gallery
        var productGallery = new ProductGallery('div#product-media > div.more-views ul li', 'div#product-media > p.product-image img');
        var popupProductGallery = new ProductGallery('div#product-gallery-container div.more-views ul li', 'div#product-gallery-container p.product-image img');

        if (productGallery.items.length == popupProductGallery.items.length)
        {
            productGallery.items
                    .find('a')
                    .each(function (index, link) {
                        jq(link).on('click', function (index) {
                            jq(popupProductGallery.items[index])
                                    .find('a')
                                    .trigger('click');
                        }.bind(this, index));
                    });
        }

        // product gallery overlay
        new Overlay('a#cloudZoom', 'div#product-gallery-container', 'h5, span.overlay-close-button');

        var tabs = jq('div.col-main div.product-tabs ul li.product-tab');
        if (tabs.length > 0)
        {
            var contents = jq('div.col-main div.product-tabs div.product-tab-content');
            if (contents.length == tabs.length)
            {
                tabs.each(function (index) {
                    jq(this).on('click', function (index) {
                        tabs.removeClass('product-tab-active');
                        contents.removeClass('product-tab-content-active');

                        jq(this).addClass('product-tab-active');
                        jq(contents[index]).addClass('product-tab-content-active');
                    }.bind(this, index));
                });

                tabs
                        .first()
                        .trigger('click');
            }
        }

        // size chart overlay
        new Overlay('a#size-chart-label', 'div#size-chart-container', 'span.overlay-close-button');
        new Overlay('#preordernote', 'div#preorder-container', 'span.overlay-close-button');
        var sliderWrapper = jq('div.box-up-sell div.slider-wrapper');
        if (sliderWrapper.length == 1)
        {
            sliderWrapper
                    .find('div.slider')
                    .jcarousel();
            sliderWrapper.find('a.slider-control')
                    .on('jcarouselcontrol:active', function () {
                        jq(this).removeClass('slider-control-disabled');
                    })
                    .on('jcarouselcontrol:inactive', function () {
                        jq(this).addClass('slider-control-disabled');
                    });
            sliderWrapper.find('a.slider-control-previous').jcarouselControl({target: '-=1'});
            sliderWrapper.find('a.slider-control-next').jcarouselControl({target: '+=1'});
        }

        // add question overlay
        new Overlay('a#add-question-label', 'div#question-form-container', 'span#overlay-close-button');
        new Overlay('a#add-size-request-label', 'div#size-request-form-container', 'span#overlay-close-button-size');
    }

    // gomage-checkout-onepage-index
    if (jq('body.gomage-checkout-onepage-index').length == 1)
    {
        // gift wrapping image overlay
        new Overlay('a#gift-wrapping-image-label', 'div#gift-wrapping-image-container', 'span.overlay-close-button');

        // gift wrapping message overlay
        new Overlay('a#write-your-message-label', 'div#allow-gift-messages-for-order-container', 'span.overlay-close-button, button');
    }

    // cookies
    //jq.removeCookie('cookie_consent', { path: '/' });

    var cookieConsent = jq.cookie('cookie_consent');
    if (cookieConsent != '1' && !jq('body').hasClass('cookie-consent-not-required'))
    {
        jq('body').addClass('cookie-consent-required');

        /*jq('a#cookie-consent-yes-button').on('click', function (event) {
         event.preventDefault();
         
         jq.cookie('cookie_consent', '1', { expires: 7, path: '/' });
         
         jq('body').removeClass('cookie-consent-required');
         
         cookieOverlay.hide();
         cookieOverlay.background.removeClass('transparent');
         cookieOverlay.wrapper.removeClass('absolute');
         });*/

        cookieOverlay.closeButtons.on('click', function () {
            jq.cookie('cookie_consent', '1', {expires: 7, path: '/'});

            jq('body').removeClass('cookie-consent-required');

            cookieOverlay.hide();
//            cookieOverlay.background.removeClass('transparent');
            cookieOverlay.wrapper.removeClass('absolute');
        });

//        cookieOverlay.background.addClass('transparent');
        cookieOverlay.wrapper.addClass('absolute');
        cookieOverlay.show();
    }

    if (jq('.homepage-top-slider').length) {
        if (jq('.homepage-top-slider .swiper-slide').length > 1) {
            var sliderTopSwiper = new Swiper('.homepage-top-slider-container', {
                pagination: '.homepage-top-slider .swiper-pagination',
                paginationClickable: true,
                lazyLoading: false,
                autoplay: 3000,
                loop: false,
                speed: 800,
                keyboardControl: true,
                grabCursor: true,
                resizeReInit: true
            });
        }
    }
});

var TopLinkPopup = function (linkSelector, popupSelector) {
    this.init(linkSelector, popupSelector);
}

jq.extend(TopLinkPopup.prototype, {
    link: null,
    popup: null,
    init: function (linkSelector, popupSelector)
    {
        this.link = jq(linkSelector);
        if (this.link.length != 1)
            return;

        this.popup = jq(popupSelector);
        if (this.popup.length != 1)
        {
            this.link.hide();

            return;
        }

        this.link.after(this.popup);
    }
});

var Overlay = function (labelSelector, containerSelector, closeButtonSelector) {
    this.init(labelSelector, containerSelector, closeButtonSelector);
}

jq.extend(Overlay.prototype, {
    background: null,
    wrapper: null,
    label: null,
    container: null,
    containerParent: null,
    closeButtons: null,
    init: function (labelSelector, containerSelector, closeButtonSelector)
    {
//        this.background = jq('div#overlay-background');
//        if (this.background.length == 0)
//        {
//            this.background = jq('<div id="overlay-background"></div>');
//
//            jq('body').prepend(this.background);
//        }

        this.wrapper = jq('div#overlay-wrapper');
        if (this.wrapper.length == 0)
        {
            this.wrapper = jq('<div id="overlay-wrapper"></div>').insertAfter("body >.wrapper");
        }

        this.label = jq(labelSelector);
        if (this.label.length == 0)
            this.label = jq();

        this.container = jq(containerSelector);
        if (this.container.length != 1)
            return;

        this.containerParent = this.container.parent();
        if (this.containerParent.length != 1)
            return;

        this.closeButtons = this.container.find(closeButtonSelector);

        this.label.on('click', function (event) {
            event.preventDefault();

            this.show();
        }.bind(this));

        this.closeButtons.on('click', this.hide.bind(this));
    },
    show: function ()
    {
        this.container.css('display', 'block');
//        this.background.show();
        this.wrapper
                .append(this.container)
                .show();
    },
    hide: function ()
    {
//        this.background.hide();
        this.wrapper.hide();
        this.container
                .appendTo(this.containerParent)
                .css('display', 'none');
    }
});

var ProductGallery = function (itemSelector, imageSelector) {
    this.init(itemSelector, imageSelector);
}

jq.extend(ProductGallery.prototype, {
    items: [],
    image: null,
    init: function (itemSelector, imageSelector)
    {
        this.items = jq(itemSelector);
        if (this.items.length == 0)
            return;

        this.image = jq(imageSelector);
        if (this.image.length != 1)
            return;

        this.items
                .find('a')
                .each(function (index, link) {
                    jq(link).on('click', this.changeImage.bind(this, index, link))
                }.bind(this));
    },
    changeImage: function (index, link, event)
    {
        event.preventDefault();

        this.items.removeClass('active');
        jq(this.items[index]).addClass('active');

        this.image.attr('src', jq(link).attr('href'));
    }
});